# TP2 - Le gestionnaire IOU

## Description

Le gestionnaire IOU se veut un outil de gestion de tout ce que vous avez. Ça peut être des outils, livres, meubles, jeux, ... vous aurez à définir au minimum 3 catégories d'emprunt que vous implanterez dans votre gestionnaire.


## Caractéristiques globales 

* On peut créer autant de fichier gestionnaire que désiré.
* On doit gérer minimalement 3 types d'objets, à vous de les personnaliser comme vous le souhaitez.
* Chaque objet d'inventaire a des identifications communes: 
  * Nom: doit être redéfini.
  * Prix
  * Quantité 
  * Date d'achat
  * Facture (img) : à afficher lors de la vue modifier, ne pas afficher dans le tableView principal.
  * Description : généré automatiquement en fonction du type d'objet. Donc un livre ce sera le nom du livre, l'auteur, l'année de date de sortie, ... Alors que votre perçeuse sans fil, aura une marque, un modèle et un # de série.
  * Emplacement (txt)
  * Les états des objets sont prédéfinis et peuvent être : 
    * En ma possession
    * Prêté
    * Perdu
    * ...

## Caractéristiques personnalisables

* Identification différente:

Par exemple, vous voulez noter les livres à l'intérieur, ils proviennent avec un auteur, une maison d'édition, ...

Alors que les outils ont une marque et un numéro de modèle.

* La fenêtre d'affichage, d'insertion et de suppression de l'objet va dépendre du type d'objet à insérer. Elle devra correspondre alors à ce qui est choisi comme type d'outil.

Vous devez au minimum avoir des Strings, Nombres (entier ou flottant) DatePicker, des ComboBox, des ListView dans votre interface parmi les propriétés intégrées ou ajoutées.

## Recherche

On doit pouvoir faire des recherches en fonction d'un mot dans sa description générale ou celle personnalisable. Par exemple, on doit pouvoir rechercher autant dans la description de l'objet que dans la marque d'un outil ou l'auteur d'un livre.

## Filtres d'ordre général dans l'interface générale. 

Au minimum, 4 types de filtres sont nécessaires

* Type d'objet (on peut afficher les outils mais pas les jeux, ni les livres). (Utiliser ici un _EnumSet_)
* Date d'achat (en utilisant un calendrier) 
* Prix (< ou > que x)
* État (Utilisez ici un _Enum_)

## Menu Fichier

### Nouveau

Crée un nouveau fichier .dat vide à l'emplacement spécifié par le _FileChooser_.

### Ouvrir

On peut ouvrir un fichier existant .dat. Si l'importation ne fonctionne pas, affichez un message approprié.

### Sauvegarder

Sauvegardez l'état actuel dans le fichier .dat

### Sauvegarder sous

Permettre à l'utilisateur de sauvegarder l'état actuel dans un autre fichier. Le nouveau fichier devient le fichier par défaut.

On doit confirmer si on doit absolument écraser un fichier.

### Exporter 
* On exporte dans un fichier .txt en format texte. Il faudra parcourir chaque objet et l'exporter.

### Images des factures
* Les images doivent être copiées dans un sous-répertoire images/ du répertoire contenant le fichier .dat. Il est donc recommandé de ne pas avoir deux fichiers .dat dans le même répertoire.

## Présentation des classes

* Vous devrez créer un grand nombre de classes. Vos méthodes devraient avoir au maximum une dizaine de lignes, sinon personnalisez des objets d'interfaces pour vous faciliter la tâche tel que présenté dans l'exemple _multiWindow_.

## Évaluation

|Sujet |Valeur   |  
|---|---|
|Affichage des données | 10  |
|Personnalisation | 10 | 
|Insertion / ~~Modification~~ / Suppression  |  10 |
| Filtres (4), Recherche et liaison avec _toolbar_ |  10 |
|Sérialisation et Menu | 10 |
|Qualité des interfaces (langue) | 10 | 
|Respect des normes (méthode concise, variable bien définie, commentaires)  |  10 |
|Tests | 5 | 
| **Total** | **75** | 

### Restriction Importante

* Vous ne devez utiliser aucun _Singleton_, vous devez passer les variables à chaque fois.
* Vous ne devez utiliser aucune variable statique, vous devez créer des instances de tous les classes nécessaires.
* Vous pouvez utiliser des méthodes statiques d'une classe statique de façon utilitaire uniquement. Votre inventaire n'est pas un objet.

## Annexes

![](TP2/%C3%89cran%20principal.png)
![](TP2/Ajout1.png)
![](TP2/Ajout2.png)
![](TP2/Ajout3.png)
![](TP2/Ajout4.png)
![](TP2/Modif1.png)

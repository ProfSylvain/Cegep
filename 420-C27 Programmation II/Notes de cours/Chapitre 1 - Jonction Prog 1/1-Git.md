# GIT

La première étape consiste à créer un compte sur GitLab. J'ai choisi GitLab car il est plus simple de faire des dépôt privés.


## Dépôt "_Repository_" 

Par la suite, vous devrez créer un dépôt (_repository_). Un dépôt représente un ensemble de répertoires et de fichiers appartenant généralement à un projet. 

Il peut représenter un seul projet _opensource_ tel que celui-ci:

https://github.com/home-assistant/core

Ou plutôt en représenter plusieurs tels que mon dépôt qui servira à ce cours:

https://gitlab.com/profsylvain/cegep

## Accès

Vous avez deux options pour vous connecter à distance:

* nom d'utilisateur / mot de passe (insécure)
* Clé privée / publique

### Génération de clé privée (à valider)

ssh-keygen

Par défaut, les clés vont se générer dans le répertoire P:\.ssh

Copiez la clé publique sur le site de gitlab dans le profil sous ssh keys.

Ceci vous permettra de vous identifier et seulement vous. Vous n'aurez pas à saisir de mot de passe.

Vous devrez transporter votre clé privée avec vous ou vous en créer de nouvelles et les charger (_uploader_) (par exemple pour votre ordinateur à la maison).

De mon côté, je prends la même.

### On clone

```
git clone git@gitlab.com/profsylvain/cegep
```

Cette commande produira une copie du dépôt cegep dans votre répertoire en cours.

Vous pourrez répliquer "_cloner_" le dépôt que vous avez créé précédemment

### git add

Pour ajouter des éléments à git à surveiller dans notre arbre local.

```
git add nom_de_fichier
ou 
git add . (répertoire)
```

### git commit

* On ajoute une version locale de notre code. Sachez qu'on peut toujours revenir en arrière en utilisant _checkout_.

* Il ne faut jamais enregistrer "_commit_" du code non-fonctionnel.

* Il faut enregistrer "_commit_" régulièrement, i.e. à chaque fin de création d'une méthode. On ne sait jamais à quel moment on va introduire un problème (_bug_) qui ne pourra être résolu qu'en revenant en arrière.

```
git commit -am "Nom de la nouvelle version"
```

### git push

Ensuite, il faut répliquer notre version actuelle sur le serveur à distance.

```
git push
```

### git pull

On obtient la dernière version du serveur à distance sur l'ordinateur local. **Fonctionne uniquement s'il y a un repository déjà configuré dans le répertoire en cours**.

```
git pull
```



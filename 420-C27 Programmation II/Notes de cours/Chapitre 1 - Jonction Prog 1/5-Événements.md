# Gestionnaire d'événement

En JavaFX, nous pouvons développer des applications graphiques (GUI). Dans ses applications, lorsqu'un utilisateur interagit avec les _nodes_ de l'application, un événement est créé et doit être géré.

## Type d'événement

* Avant-plan (_Foreground_): sont produits lors d'une intéraction directe avec l'utilisateur: un clic sur un bouton, choisir un élément dans une liste, dérouler une page, entrer un caractère au clavier.

* Arrière-plan (_Background_): Ils dépendent plus du système d'exploitation que de l'utilisateur. 

## Multitudes d'événements gérables en JavaFX:

Voici une liste incomplète des types d'événements gérables en JavaFX:

* _ActionEvent_ - Type d'événement lorsqu'un bouton d'interface est appuyé.
* _MouseEvent_ - Type d'événement qui survient lorsqu'un bouton (de la souris). Plusieurs potentiels: enfoncé, relâché, déplacé, entre dans une zone, sort d'une zone, ...
* _KeyEvent_ - Type d'événement qui survient lorsqu'un bouton (du clavier) survient sur une _node_. Plusieurs potentiels: touche pressée, touche relâchée, type de touche.
* _DragEvent_ - Type d'événement qui survient lors d'un glissement d'un objet. Supporte ce qu'on appelle le _Drag & Drop_, ...
* _Window Event_ - Type d'événement qui survient par rapport à la gestion de la fenêtre: affichée, cachée, ...

## Gestion des événements

La gestion des événements est le mécanisme qui contrôle et décide ce qui devrait se produit lors d'un événement. Le mécanisme à le code nécessaire à faire exécuter lorsqu'un événement survient.

JavaFX fournit les *handlers* et les *filters* nécessaire pour les gérer. En JavaFX, tous les événements ont:

* Un type: Qu'est-ce qui s'est produit? Dans le cas d'un _mouseEvent_, ça peut être pressé, relâché, ...
* Une source: Qu'est-ce qui produit l'événement. C'est l'origine de l'événement. Dans le cas d'un _mouseEvent_, c'est la souris qui est la source.
* Une cible: Sur quoi survient l'événement. Ça peut être une _node_, une scène ou une fenêtre.

## Les étapes de la gestion en JavaFX

1. Construction de la route
   
La chaîne de gestion est crée afin de déterminer le chemin approprié pour un événement, peu importe quand il est généré. Il descend à partir du stage vers le _node_ sur lequel l'événement est généré.

![](images/javafx-event-handling.png)

2. Capture de l'événement

L'événement est émis à partir du haut vers la cible (_node_). Si un des _nodes_ sur le chemin de la destination a le filtre, le filtre est appelé et l'événement est passé au suivant. Si aucun filtre ne consomme l'événement, la cible va le recevoir éventuellement et le traiter.

3. Les bulles de l'événement

Lorsque la cible est rejointe, l'événement fait le chemin inverse et part de la cible et remonte vers la racine de l'application. Si un des _nodes_ a un gestionnaire implémenté pour gérer l'événement, il est appelé. Lorsque complété, l'événement continue son petit bonhomme de chemin vers le haut de la chaine. Si aucun gestionnaire d'événement n'est configuré, la racine (stage) recevra l'événement et le gèrera.

## Gestion des événements

### _Filters_

Les filtres s'exécutent pendant l'opération de capture. Les filtres permettent d'être avisé lorsque une situation se produit et ultimement peut empêcher qu'un événement continue à descendre et soit capturé.

```java
button.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
    @Override
    public void handle(MouseEvent mouseEvent) {
            button.setEffect(shadow);
    }
});
```

### _Handlers_

Un _handler_ est exécuté pendant la phase de bulle. Si un gestionnaire d'événement pour un _node_ n'a pas consommé l'événement, un _node_ parent pourrait alors prendre en charge le traitement. 

```java
button.setOnAction(new EventHandler<ActionEvent>() {
    @Override
    public void handle(ActionEvent actionEvent) {
        System.out.println(actionEvent);
    }
});
```

La différence principale est que les filtres peuvent empêcher les _handlers_ d'agir.



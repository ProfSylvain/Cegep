# _Listener_

Habituellement en programmation un _EventHandler_ et un _ChangeListener_ sont très similaires, cependant JavaFx semble avoir séparé ces deux concepts.

Donc si l'on comprend bien, un _EventHandler_ comme nous avons vu dernièrement découle d'une intéraction utilisateur (clic sur un bouton, _mouse over_, _mouse drag_, _mouse drop_, clé du clavier, ...)

Chaque propriété d'un élément d'interface offre des méthodes d'observations pour déterminer si une de ces _valeursObservableList_ est modifiée. Nous pouvons alors écouter pour des changements par l'entremise du _ChangeListener_

Pour un _slider_ par exemple:

```java
// Listen for Slider value changes
slider.valueProperty().addListener(new ChangeListener<Number>() {
	@Override
	public void changed(ObservableValue<? extends Number> observable,
			Number oldValue, Number newValue) {
		
		System.out.println("Nouvelle valeur (newValue: " + newValue.intValue() + ")\n");
	}
});
```

Ça semble un peu complexe, mais comme toujours, c'est relativement la même chose qu'un _EventHandler_

* _slider.valueProperty()_ nous donne accès à la propriété _value_. Il y a plusieurs autres propriétés disponibles telles que _focusedProperty_ ou _disabledProperty_, toutes deux pourraient être observés à partir d'un _ChangeListener_
* _addListener(...)_ s'attend à recevoir un objet de type _ChangeLister_ de type _Number_. _ChangeListener_ est une interface donc on s'attend à créer un objet qui implémente les besoins de _ChangeListener_, i.e. la méthode _changed_
* la méthode _changed_ va se faire appeler à chaque fois qu'un changement survient.


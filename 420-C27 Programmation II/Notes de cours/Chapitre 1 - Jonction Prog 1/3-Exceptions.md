# Exceptions

Les méthodes en Java utilisent les exceptions pour indiquer aux fonctions appelantes qu'un problème est survenu. Comment savoir qu'une méthode peut retourner une exception? En jetant un coup d'oeil sur sa déclaration.

En tant que développeur vous devez être en mesure de lire la documentation Java et de la comprendre, bien sûr qu'il y aura encore des concepts un peu vague, mais ça viendra avec le temps. 

La source d'information principale pour un développeur devrait être la documentation et non ~~StackOverflow~~

https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/io/FileWriter.html

Si vous observez le constructeur de la classe _FileWriter_, vous allez remarquez qqch de spécial dans sa définition:

![](images/FileWriter.png)

**Throws**

**IOException - if the named file exists but is a directory rather than a regular file, does not exist but cannot be created, or cannot be opened for any other reason**

Ceci indique que dans le cas d'un problème, la méthode peut retourner une exception. 

Le compilateur va vouloir que vous validiez alors dans un _try-catch_ l'appel à cette méthode. Prenons une méthode que vous connaissez probablement:

https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/util/Scanner.html#nextInt()

Trois exceptions sont possibles:

```java
Throws:
InputMismatchException - if the next token does not match the Integer regular expression, or is out of range
NoSuchElementException - if input is exhausted
IllegalStateException - if this scanner is closed
```

Maintenant vous comprenez pourquoi vous encerclez le test de lecture de l'entrée de la ligne de commande dans un 

```java
try {
    // Faire des trucs dangereux, par exemple:

    Scanner inputFromCommandLine = Scanner(System.in);
    int age = inputFromCommandLine.nextInt();
    System.out.println("Bonjour");
}
catch (InputMismatchException e) {
    ...
}
catch (NoSuchElementException e) {
    ...
}
catch (IllegalStateException e) {
    ...
}
```

Bien sûr toutes les applications que vous avez programmés à ce jour ont toutes les _catchs_ nécessaires pour gérer les exceptions de lecture. ;)

Est-il possible de savoir si la prochaine donnée lue sera un entier sans utiliser le InputMismatchException ?

Oui, dans la classe Scanner, il y a une méthode qui existe qui permet de déterminer si la valeur reçue est un entier:

https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/util/Scanner.html#hasNextInt(int)

## Les règles des exceptions

1. Sans un _try_, pas de _catch_ ni de _finally_
2. Aucun code entre un _try_ et un _catch_
3. Un _try_ doit être suivi par un _catch_ ou un _finally_, mais probablement, sans _catch_ votre professeur va vous enlever des points.
4. **Il ne faut pas utiliser les exceptions comme un traitement normal de l'application, c'est une exception.**

## Retarder une exception

Si vous créez une méthode et que vous ne voulez pas gérer les exceptions, vous pouvez retarder l'inévitable, c'est-à-dire à forcer les gens utilisant votre méthode à déclarer les _throws_.


```java
public static void uneMethode(int a, int b) throws UneTresTresTresMauvaiseException {

}

public static void main() {
    try {
        uneMethode(3,4);
    }
    catch (UneTresTresTresMauvaiseException e) {
        System.out.println("On doit bien gérer l'exception quelque part");
    }
}
```

# Types d'exception

Il y a deux types d'exceptions:

## _Checked Exception_

Aussi appelées exceptions de compilation, elles ne peuvent être ignorées lors de la compilation.

```java
import java.io.File;
import java.io.FileReader;

public class FilenotFound_Demo {

   public static void main(String args[]) {      
      File file = new File("C://file.txt");
      FileReader fr = new FileReader(file);    
   }
}
```

_FileReader_ sera souligné en rouge afin de vous offrir une suggestion par rapport à votre code. Généralement, il est bien de lire les suggestions et de se renseigner. Si vous l'ignorez:

```
../../420-C27 Programmation II/Exemple/Chapitre 1/Exception/compileTimeException/src/Main.java:6:25
java: unreported exception java.io.FileNotFoundException; must be caught or declared to be thrown
```

Le message d'erreur est assez clair.

## Unchecked exceptions

Une expression _unchecked_ ou _Runtime_ est une exception qui ne peut se produire qu'à l'exécution. La majorité des exceptions que vous avez croisé se situent ici. 

Elles représentent les cas suivants:

* Erreurs de programmation
* Erreurs de logique
* Erreurs d'appel API
* ...

Par exemple, essayez d'atteindre le 6ième élément d'un _array_ de cinq éléments. On obtient alors le _ArrayIndexOutOfBoundsException_:

```java
try {
    int[] i = new int[5];
    i[5] = 3;
}
catch (Exception e){
    System.out.println(e.getMessage());
    System.out.println(e.getClass());
}
```

```
./../420-C27 Programmation II/Exemple/Chapitre 1/Exception/runtimeException/src/Main.java
Index 5 out of bounds for length 5
class java.lang.ArrayIndexOutOfBoundsException
```

## Dans les tests unitaires

Il est possible de tester à savoir quel type d'exception une méthode retourne:

```java
@org.junit.jupiter.api.Test
void somme1() {
    Exception exception = assertThrows(Exception.class, () -> {
            Main.somme(Integer.MAX_VALUE, 1);
    });

    String expectedMessage = "Dépassement de capacité";
    String actualMessage = exception.getMessage();

    assertTrue(actualMessage.contains(expectedMessage));
}
```

Dans un premier cas on remarque qu'il est possible de s'attendre à la sortie d'une exception de la part d'un bout de code:

assertThrows a comme arguments:

* le type d'exception générée
* par l'appel du second élément
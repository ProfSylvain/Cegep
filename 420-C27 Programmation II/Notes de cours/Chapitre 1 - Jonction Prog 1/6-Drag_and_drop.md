# Implémentation d'un _drag and drop_

L'exemple d'aujourd'hui est disponible sur le site d'Oracle:

https://docs.oracle.com/javafx/2/drag_drop/jfxpub-drag_drop.htm

1. Débuter

```java
final Text source = new Text(50, 100, "DRAG ME");
final Text target = new Text(300, 100, "DROP HERE");
```

2. Détecter le début, cest de type _MouseEvent_ sur un objet qui est la source.

```java
source.setOnDragDetected(new EventHandler<MouseEvent>() {
    public void handle(MouseEvent event) {
        /* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);
        
        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        content.putString(source.getText());
        db.setContent(content);
        
        event.consume();
    }
});
```

3. On doit savoir si on peut relâcher sur un objet.

```java
target.setOnDragOver(new EventHandler<DragEvent>() {
    public void handle(DragEvent event) {
        /* data is dragged over the target */
        /* accept it only if it is not dragged from the same node 
         * and if it has a string data */
        if (event.getGestureSource() != target &&
                event.getDragboard().hasString()) {
            /* allow for both copying and moving, whatever user chooses */
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }
});
```


4. Effet visuel

```java
target.setOnDragEntered(new EventHandler<DragEvent>() {
    public void handle(DragEvent event) {
    /* the drag-and-drop gesture entered the target */
    /* show to the user that it is an actual gesture target */
         if (event.getGestureSource() != target &&
                 event.getDragboard().hasString()) {
             target.setFill(Color.GREEN);
         }                
         event.consume();
    }
});

target.setOnDragExited(new EventHandler<DragEvent>() {
    public void handle(DragEvent event) {
        /* mouse moved away, remove the graphical cues */
        target.setFill(Color.BLACK);
        event.consume();
    }
});
```

5. Drop sur la destination

```java
target.setOnDragDropped(new EventHandler<DragEvent>() {
    public void handle(DragEvent event) {
        /* data dropped */
        /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
           target.setText(db.getString());
           success = true;
        }
        /* let the source know whether the string was successfully 
         * transferred and used */
        event.setDropCompleted(success);        
        event.consume();
     }
});
```

6. On modifie la source afin de retirer l'élément _drag & drop_

```java
    source.setOnDragDone(new EventHandler<DragEvent>() {
        public void handle(DragEvent event) {
            /* the drag and drop gesture ended */
            /* if the data was successfully moved, clear it */
            if (event.getTransferMode() == TransferMode.MOVE) {
                source.setText("");
            }
            event.consume();
        }
    });
```
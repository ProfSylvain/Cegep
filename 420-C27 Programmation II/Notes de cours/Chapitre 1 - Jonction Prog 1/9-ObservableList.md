# _ObservableList_

Soit le _ComboBox_ suivant:

```java
    ComboBox<String> emailComboBox = new ComboBox<String>();
    emailComboBox.getItems().addAll("A","B","C","D","E");
    emailComboBox.setValue("A");
    System.out.println(emailComboBox.getValue());
```

Deux éléments importants:

  1. \<String\> indique le type des objets qui se retrouveront dans le _ComboBox_, puisqu'ici on utilise _String_, ce sera des _String_ qui seront à l'intérieur.

  2. Que retourne la méthode _getItems_ selon vous? Il ne s'agit pas juste d'un _array_ d'éléments mais bien d'une liste d'éléments observée nommée _ObservableList_.


### Type de sélection

Dans une liste donnée, il est possible de sélectionner un ou plusieurs éléments:

```java
ListView<String> list = new ListView();

// Simple

listview.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

// Plusieurs 

listview.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
```

## Pourquoi utiliser un _ObservableList_ ?

Nous pouvons y rattacher des _Listener_ pour constater le changement d'élément de la liste.

```java
listview.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<String>() {
    @Override
    public void onChanged(Change<? extends String> change) {
        ...
    }
}
```
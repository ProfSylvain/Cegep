# _EventHandler_, que peut-on faire avec?

Depuis le début nous créons nos événements directement:

```java
button.setOnAction(new EventHandler<ActionEvent>() {
    @Override
    public void handle(ActionEvent actionEvent) {
        System.out.println(actionEvent);
    }
});
```

Idem pour les différents types d'événements, que ce soit un _mouseEvent_ ou un _keyEvent_. 

Mais pourrions nous les créer à l'extérieur de ce bout de code là et donc de les assigner à plusieurs?

```java
EventHandler<ActionEvent> clickOnButton = new EventHandler<ActionEvent>() {
    @Override
    public void handle(ActionEvent actionEvent) {

        try {
            int a = Integer.parseInt(txt1.getText());
            int b = Integer.parseInt(txt2.getText());

            lbl.setText(String.valueOf(a+b));
        }
        catch (NumberFormatException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de transtypage");
            alert.setContentText("Erreur de transtypage.");
            alert.show();
        }
    }
};
```

Le mode de création reste sensiblement le même, cependant, le gros avantage c'est qu'on peut l'affecter à plusieurs éléments :

```java
btn1.setOnAction(clickOnButton);
btn2.setOnAction(clickOnButton);
```

## C'est bien beau tout ça, mais comment faire pour identifier le bon objet?

Nous recevons dans nos événements _handle_, l'objet: _ActionEvent_

```java
...
public void handle(ActionEvent actionEvent) {
...
```

Que contient-il? Comment faire pour l'identifier? Essayez avec _getTarget_ ;)

```java
if (actionEvent.getTarget() instanceof Button) {
    Button btn = (Button) actionEvent.getTarget();
...
```
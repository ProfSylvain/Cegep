
# Normes de programmation

* Dans le cycle de vie d'un produit logiciel, la phase de maintenance représente la majeure partie du temps (environ 80%), un logiciel est rarement développé par une seule personne.
* Plusieurs personnes vont lire le code et seront obligées de le comprendre.
* Règle générale, ce ne sont pas ceux qui ont procédé à sa création; leur temps d'adaptation avant une pleine productivité dépend de leur capacité à comprendre le code source et à assimiler la documentation relative au projet.
* La réussite d'un projet logiciel dépend des moyens mis en oeuvre pour assurer une consistance dans le codage.
* Des conventions strictes respectées par tous sont nécessaires.
* Des outils de développement proposent certaines fonctionnalités qui facilitent la cohérence du code source; ces fonctionnalités ne sont pas suffisantes
nous allons, durant les prochains cours, voir des directives ou des stratégies appelées "les bonnes pratiques de la programmation".

## Nom des variables: significatif

Un nom significatif est un nom auquel une personne ne connaissant pas votre programme pourra deviner ce que signifie cette variable. 

* Le nom doit être à la fois explicite (dénoter le contenu) et court
    * _nombreEtuds_ au lieu de _compteur_
    * _perimetre_ au lieu de _lePerimetreDuCercle_
* Être descriptif pour distinguer des variables reliées
* _ancienSolde_ et _nouveauSolde_ au lieu de _solde1_ et _solde2_
* Ne pas inclure le type de la variable dans son nom
    * _poids_ au lieu de _poidsFloat_
* Les noms de variable doivent être en _camel case_
    * _nouveauSolde_ au lieu de _NouveauSolde_

Demandez à votre voisin le cas échéant s'il comprend à quoi sert votre variable dans votre code, si oui, vous avez un bon nom de variable, sinon vous devriez le changer.

Pour les itérations, les i, j, k sont autorisés.

~~commenter chaque variables~~

## Méthodes  

### Nom des méthodes

Une méthode doit être définie pour ne faire qu'une seule opération et la faire correctement. Donc le nom devrait représenter ce qu'elle fait parfaitement.

Si votre méthode fait trop d'opérations, sindez-la en petits morceaux.

## Argument des méthodes

* Le nombre d'arguments devrait être limité à 3 ou 4 (grand maximum). Sinon, on vient à se demander ce que fait votre méthode si elle a besoin d'autant de paramètres pour fonctionner.

* Si vous n'arrivez pas à réduire, p-e créer un objet qui conserve les 3 ou 4 éléments que vous tentez de passer. (nous y reviendrons plus tard quand nous nous attaquerons au chapitre ces classes).

## GUI

Les objets d'interface sont généralement nommés avec un préfixe:

```
btn - Button
chk - CheckBox
clr - ColorChooser
cmb - ComboBox
ico - DesktopIcon
edt - EditorPane
fch - FileChooser
ifr - InternalFrame
lbl - Label
lyp - LayeredPane
lst - List
mnu - MenuBar
mni - MenuItem
opt - OptionPane
pnl - Panel
pmn - PopupMenu
prg - ProgressBar
rad - RadioButton
rot - RootPane
scb - ScollBar
scr - ScrollPane
spr - Separator
sld - Slider
spn - Spinner
spl - SplitPane
tab - TabbedPaneJTable
tbl - Table
tbh - TableHeader
txa - TextArea
txt - TextField
txp - TextPane
tgl - ToggleButton
tlb - ToolBar
tlt - ToolTip
tre - Tree
vpr - Viewport
win - Window and descendants (JFrame, JDialog, JFileDialog)
```

Source: http://zuskin.com/java_naming.htm


## Événements GUI

On nomme l'événement dans le nom de la méthode:

btnNom_onClick();

## Aération, espacement, indentation

Un code aéré et bien indenté est plus facile à lire et à comprendre. Respectez et apprenez les raccourcis pour indenter votre code.

## Nommer les projets avec des noms représentatifs

N'oubliez pas que les projets que vous aurez à réaliser seront stockés dans votre dépôt GitLab, il sera alors rattaché à votre nom.

## Internationalisation

Affichez toujours de façon à utiliser le système canadien de représentation des dates, des nombres à virgules, ...

```java
import java.text.*;
import java.util.*;
  
class NumberFormatDemo {
    public static void main(String[] args)
    {
        // représentation pour différents pays
        double d = 123456.789;
        numberFormat nf
            = numberFormat.getInstance(Locale.ITALY);
        numberFormat nf1
            = numberFormat.getInstance(Locale.US);
        numberFormat nf2
            = numberFormat.getInstance(Locale.CHINA);
  
        System.out.println("ITALIE représentation de " + d
                           + " : " + nf.format(d));
  
        System.out.println("États-Unis représentation de " + d
                           + " : " + nf1.format(d));
  
        System.out.println("CHINE représentation de " + d
                           + " : " + nf2.format(d));
    }
}
```

# Types de variable

* boolean 
* byte 
* char  
* short  
* int  
* long  
* float 
* double

```java
boolean majeur;
if (age >= 18 == true)
  majeur = true;
else
  majeur = false;

majeur = age >= 18;
```

## Boucles 

```java
do {
} while (cond);

while (cond){
}

for (int i=0; i<x; i++){}
```

### Choix entre _for_ et _while_???

Lorsqu'on sait le nombre de fois qu'on va boucler, nous choisissons toujours le _for_, sinon le _while_ ou le _do_. 

La différence entre le _while_ et le _do_, on effectue au moins une fois la boucle avec le _do_, puisque que la condition est à la fin.

## Tableaux 1d statiques

```java
int[] tabInt = {99,12,3,16,12,13,24,32,1,-55};
int[] tab = new int[3];
tab[0] = 7;
tab[1] = 4;
tab[2] = tab[0] + tab[1];
int nbElements = tab.length;
```

## Lecture d'entrée clavier
```java
Scanner scan = new Scanner(system.in);
try {
    age = scan.nextInt()
} catch (InputMismatchException e){ //une exception est levée
    System.out.println("Erreur type de données");
}
```
* Seul un _catch_ permet d'attraper une exception. 
* Il est nécessaire de maitriser les exceptions et comment les gérer en programmant. Un _if_ ne permet pas d'attapper une exception.
* Vous vous rappelez les interruptions vues en système d'exploitation 1, une exception est une interruption de code.
* Il est **IMPÉRATTIF** de bien nommer le type d'exception que l'on tente d'attraper.
* **L'exception est le cas extrême, il est préférable d'utiliser plusieurs _if_ pour éviter les exceptions plutôt que d'en générer une grande quantité.**

## Chaînes

String vs char[]

https://www.w3schools.com/java/java_strings.asp


```java
String nom = "toto";
char c = nom.charAt(0);
char[] tab = nom.toCharArray();
int i = nom.indexOf("to");
boolean ok = nom.equals("toto");
```

### Classes enveloppes

```java
int x = Integer.parseInt("1");
double d = Double.parseDouble("3,4324"); //NumberFormatException
double d = Double.parseDouble("3.4324");
String valeur = String.valueOf(d);
```

## Notions variables globales vs locales

Il est important de limiter l'utilisation des variables globales, car elles restent en mémoire tout au long de l'exécution de notre programme. Alors que les variables locales des méthodes seront libérés lorsque la méthode quittera et lorsque la variable n'est plus nécessaire par le logiciel.

Une variable globale a le préfix static dans sa déclaration et est accessible de partout dans la classe peu importe la méthode. C'est à proscrire comme façon de développement

## Algorithmie tri par bulle

```java
private static void permute(int[] tab, int i1, int i2){
    int transit = tab[i1];
    tab[i1] = tab[i2];
    tab[i2] = transit;
}

public static void triUpBubble(int[] tab){
    for (int i=0; i<tab.length-1; i++)
        for (int j=i+1; j<tab.length; j++)
            if (tab[j] < tab[i])
                permute(tab, i, j);
}

public static void main(String[] args) {
        int[] t = {6,4,3,1,2,8,7};
        triUpBubble(t);
        System.out.println(Arrays.toString(t));
}

```

## Surcharge méthode

La surcharge d’une méthode ou d’un constructeur permet de définir plusieurs fois une même méthode/constructeur avec des arguments différents. Le compilateur choisit la méthode qui doit être appelée en fonction du nombre et du type des arguments.

```java
public class Utils {
    public static int somme(int n1, int n2){
        return n1 + n2;
    }

    public static double somme(double n1, double n2){
        return n1 + n2;
    }
}
```

## Fichier utilitaire
```java
public class Utils {
    public static int somme(int n1, int n2){
        return n1 + n2;
    }

    public static double division(double n1, double n2){
        if (n2 != 0)
            return n1 / n2;
        else
            return 0;
    }
}
```

## Test unitaire
```java
class UtilsTest {

    @Test
    void somme() {
        assertEquals(5, Utils.somme(4, 1));
    }

    @Test
    void division() {
        //oracle 1
        assertEquals(2.5, Utils.division(5, 2));

        //oracle 2 division par zéro
        assertEquals(0, Utils.division(5, 0));
    }
}
```

## Tests unitaires

Le test unitaire s’assure que chaque unité se comporte comme elle le doit.

* Ces tests sont automatiques c’est à dire qu’ils n'ont pas besoin de l’intervention humaine pour savoir qu’un problème est survenu.
* On lance fréquemment les tests, par exemple à chaque modification pour détecter les problèmes dès leur apparition.
* Les tests ne sont jamais distribués aux utilisateurs.


## JUnit5 @Test

```
JUnit5 @Test (annotation au dessus des méthodes de test)
```

## Méthodes disponibles
* assertTrue(boolean b) vérifie que b est vrai
* asserNull(Object o) vérifie que o est nul
* assertEquals(Object a, Object b) vérifie l’égalité entre les deux objets
* assertEquals(int a, int b) vérifie que a et b sont égaux
* assertEquals(double a, double b, double d) vérifie que la différence entre a et b est inférieure à d
* assertEquals(expectedValue, actual value)
* assertArrayEquals(array1, array2)

```java
public static int somme(int n1, int n2){
   return n1 + n2;
}

public static double division(double n1, double n2) {
   if (n2 != 0)
     return n1 / n2;
   else
     return 0;
}

@Test
void somme(){
   //oracle 1
   assertEquals(5, somme(1, 4));
}

@Test
void division(){
    //oracle 1
    assertEquals(2.5, Utils.division(5, 2));

    //oracle 2 division par zéro
    assertEquals(0, Utils.division(5, 0));
}
```

Exemple avec utilitaire Tableaux.java:

```java
public class TableauxTest {

    @Test
    public void trierCroissant() {
        int[] tab = {5,4,3,22,-10,250};
        int[] expectedTab = {-10,3,4,5,22,250};
        Tableaux.trierCroissant(tab);
        assertArrayEquals(expectedTab, tab);
    }

    @Test
    public void maximum() {
        //oracle 1: max fin
        int[] tab = {5,4,3,22,-10,250};
        int exptextedResult = 250;
        int result = Tableaux.maximum(tab);
        assertEquals(exptextedResult, result);

        //oracle 2: max début
        tab[0] = 555;
        exptextedResult = 555;
        result = Tableaux.maximum(tab);
        assertEquals(exptextedResult, result);

        //oracle 3: max centre
        tab[tab.length % 2] = 5000;
        exptextedResult = 5000;
        result = Tableaux.maximum(tab);
        assertEquals(exptextedResult, result);
    }
}
```

* À partir de la classe à tester ici Tableaux.java, cliquer droit, choisir ** Generate **, ** Test **
* Il est important de spécifier JUnit5.
* Une classe de test sera créé TableauxTest.java
* Faire jeu de tests unitaires sur les méthodes de la classe Tableaux.java

Faire atelier 1 
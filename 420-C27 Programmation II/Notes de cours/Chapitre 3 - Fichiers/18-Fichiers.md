# Lecture d'un fichier texte

Voici quelques classes afin de faciliter la gestion d'un fichier

## _BufferedReader_

Cette façon de faire permet de lire un flux orienté caractères. Il utilise un tampon (_buffer_) pour améliorer la lecture des caractères, tableaux et lignes. En général chaque opération de lecture occasionne un appel au flux de données, cependant, en l'enveloppant (_wrapper_) dans un _BufferedReader_, on réduit leur nombre. On suggère alors d'utiliser un _BufferedReader_ autour de tout _Reader_ qui effectue des lectures qui pourraient être très gourmandes en ressources telles que _FileReader_ et _InputStreamReader_.

```java
BufferedReader in = new BufferedReader(Reader in, int size);
```

## _FileReader_

Il s'agit d'une classe afin de lire les fichiers.  Le constructeur assume un encodage de caractères par défaut et s'assure de la taille du tampon appropriée.

### Constructeurs
```java
FileReader(File file)
FileReader(FileDescriptor fd)
FileReader(String fileName)
```

## _Scanner_  

Un simple lecteur qui permet de lire et d'identifier les types primitifs ou les chaînes en utilisant les expressions régulières.

## _Files_

```java
Files.readAllLines(filepath, charset); // retourne une liste de String
Files.readAllBytes(Paths.get(fileName)); // retourne un tableau de Byte
Files.lines avec un foreach
````

### Exemple

```java
try {
    txaContent.setText("");

    FileReader fileReader = new FileReader("abc.txt");
    BufferedReader buffReader = new BufferedReader(fileReader);

    StringBuilder sb = new StringBuilder();
    String line = buffReader.readLine();

    while (line != null) {
        sb.append(line);
        line = buffReader.readLine();
    }

    txaContent.setText(sb.toString());

    buffReader.close();
    fileReader.close();

} catch (IOException e) {
    e.printStackTrace();
}
```

# Écriture d'un fichier texte

## _FileWriter_

Moyen le plus simple d'écrire un fichier en Java. Il écrit directement dans un fichier et devrait être utilisé quand le nombre d'écriture est bas. Il n'utilise pas de buffer.

## _BufferedWriter_

Similaire à _FileWriter_ mais il utilise des tampons internes afin d'écrire des données dans un fichier.

## _FileOutputStream_

L'écriture de fichier autre que texte.

## _Files_

Une classe utilitaire que vous devriez utiliser avec parcimonie.

### Exemple

```java
FileWriter fileWriter = new FileWriter("abc.txt");
BufferedWriter buffWriter = new BufferedWriter(fileWriter);

buffWriter.write(txaContent.getText());

buffWriter.close();
fileWriter.close();
```

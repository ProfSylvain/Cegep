# Enum

Dernières notions... probablement!
Bravo! :D

Un _Enum_ est un type spécial nous permettant d'avoir accès à un nombre de constantes, plutôt que d'utiliser les entiers pour représenter un ensemble de choix.

Un exemple commun est d'utiliser les valeurs NORTH, SOUTH, EAST, WEST en un ENUM.

Puisque ce sont des ENUMS, nous tappons les noms en utilisant des majuscules.

```java
public enum Day { 
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY }
```

Vous devriez utiliser un ENUM lorsque vous utilisez un ensemble de constantes. Ceci inclut également la liste des planètes de notre système solaire ou toutes données connues à la compilation: choix d'un menu, paramètres en ligne de commande. 

```java
public class EnumTest {
    Day jour;
    
    public EnumTest(Day jour) {
        this.jour = jour;
    }
    
    public void tellItLikeItIs() {
        switch (jour) {
            case MONDAY:
                System.out.println("Les lundis sont difficiles");
                break;
                    
            case FRIDAY:
                System.out.println("Les vendredis sont mieux");
                break;
                         
            case SATURDAY: case SUNDAY:
                System.out.println("La fin de semaine c'est l'idéal");
                break;
                        
            default:
                System.out.println("Le milieu de la semaine c'est moyen...");
                break;
        }
    }
    
    public static void main(String[] args) {
        EnumTest firstDay = new EnumTest(Day.MONDAY);
        firstDay.tellItLikeItIs();
        EnumTest thirdDay = new EnumTest(Day.WEDNESDAY);
        thirdDay.tellItLikeItIs();
        EnumTest fifthDay = new EnumTest(Day.FRIDAY);
        fifthDay.tellItLikeItIs();
        EnumTest sixthDay = new EnumTest(Day.SATURDAY);
        sixthDay.tellItLikeItIs();
        EnumTest seventhDay = new EnumTest(Day.SUNDAY);
        seventhDay.tellItLikeItIs();
    }
}

La sortie est:

Les lundis sont difficiles
Le milieu de la semaine c'est moyen...
Les vendredis sont mieux
La fin de semaine c'est l'idéal
La fin de semaine c'est l'idéal
```

## Classes vs _Enums_ 

Les _Enums_ peuvent avoir des méthodes et des propriétés comme les classes, ils doivent uniquement être finaux:

* Le constructeur doit être privé à l'intérieur.
* Le compilateur ajoute une méthode _values_ qui permet de boucler sur chacun des éléments.
* On ne peut ajouter d'interface à un _Enum_.


## Comment sont conservées les données dans un _Enum_ traditionnel ?

Généralement par un entier commençant par 0, jusqu'au nombre d'éléments dans le _Enum_

Afin d'optimiser nos _Enums_, nous pourrions utiliser _EnumSet (HashSet)_ et _EnumMap (HashMap)_ deux classes abstraites permettant de gérer des _Enums_ en _bitwise_.

# _EnumSet_

C'est une classe abstraite, il y a deux classes qui l'implémente:

* _RegularEnumSet_: utilise un long pour représenter chacun des objets à l'intérieur de l'_EnumSet_. Chaque bit représente un élément, comme le _long_ a 64 bits, il peut conserver jusqu'à 64 éléments.

* _JumboSet_: utilise un array pour enregistrer les éléments permettant alors plus de 64 éléments.

```java

import java.util.EnumSet; 
enum example  
{ 
    one, two, three, four, five 
}; 
public class main 
{ 
    public static void main(String[] args)  
    { 
        // Creating a set 
        EnumSet<example> set1, set2, set3, set4; 
  
        // Adding elements 
        set1 = EnumSet.of(example.four, example.three,  
                          example.two, example.one); 
        set2 = EnumSet.complementOf(set1); 
        set3 = EnumSet.allOf(example.class); 
        set4 = EnumSet.range(example.one, example.three); 
        System.out.println("Set 1: " + set1); 
        System.out.println("Set 2: " + set2); 
        System.out.println("Set 3: " + set3); 
        System.out.println("Set 4: " + set4); 
    } 
} 
```

## _EnumMap_

_EnumMap_ est un mix entre un _Enum_ et un _HashMap_, mais l'avantage c'est qu'il est hautement performant:

* Les clés sont un _Enum_.
* Ne permet pas les null
* Ordonné
* Représenté par des tableaux, donc très compact et rapide.


```java
import java.util.EnumMap;
 
enum Jours {
    LUNDI, MARDI, MERCREDI, JEUDI, VENDREDI, SAMEDI, DIMANCHE
}
 
public class EnumMapExample {
    public static void main(String[] args) {
        EnumMap<Jours, String> schedule = new EnumMap<>(Jours.class);
         
        // Ajout d'éléments à EnumMap
        schedule.put(Jours.LUNDI, "Travail");
        schedule.put(Jours.MARDI, "Travail");
        schedule.put(Jours.MERCREDI, "Étude");
        schedule.put(Jours.JEUDI, "Étude");
        schedule.put(Jours.VENDREDI, "Congé");
         
        // Récupère les éléments de EnumMap
        System.out.println(schedule.get(Jours.LUNDI)); // Sortie: Travail
        System.out.println(schedule.get(Jours.VENDREDI)); // Sortie: Congé
    }
}
```


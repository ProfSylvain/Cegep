# Héritage

L'héritage est une façon de créer de nouvelles classes basées sur des classes existantes. Il est possible ainsi de réutiliser du code déjà écrit.

Chaque classe créé à partir d'une classe existante se nomme classe enfant. Chaque classe existante est appelée classe parent.

Voici les critères pour la situation:

* Toutes les données et les méthodes de la classe parent sont également disponibles pour la classe enfant.
* Les membres privés de la classe parent ne peuvent être accédés par la classe enfant.
* La classe enfant peut accéder aux membres publics de la classe parent.

L'écriture se fait ainsi: 

```java
class enfant extends parent()

class Animal {

  public void animalSon() {
    System.out.println("Les animaux font un son");
  }
}

class Cochon extends Animal {
  @Override
  public void animalSon() {
    System.out.println("Le cochon fait: groin groin");
  }
}

class Chien extends Animal {
  @Override
  public void animalSon() {
    System.out.println("Le chien fait: wouf wouf");
  }
}

class Main {
  public static void main(String[] args) {
    Animal monAnimal = new Animal();  
    Animal monCochon = new Cochon();  
    Animal monChien = new Chien();  
    monAnimal.animalSon();
    monCochon.animalSon();
    monChien.animalSon();
  }
}
```

## @Override

Permet de spécifier une nouvelle méthode, dans une classe enfant, de même signature qui remplacera celle du parent. Il demeure possible d'appeler celle du parent en utilisant le super. Ceci conserve l'idée de polymorphisme que permet l'orienté-objet, c'est à dire, avoir plusieurs méthodes ayant le même nom dans des classes différentes.

```java
class A
{
    void m1()
    {
        System.out.println("Dans la méthode m1 de la classe A");
    }
}
  
class B extends A
{
    @Override
    void m1()
    {
        System.out.println("Dans la méthode m1 de la classe B");
    }
}
  
class C extends A
{
    @Override
    void m1()
    {
        System.out.println("Dans la méthode m1 de la classe C");
    }
}
  
class Dispatch
{
    public static void main(String args[])
    {
        // Objet de type A
        A a = new A();
  
        // Objet de type B
        B b = new B();
  
        // Objet de type C
        C c = new C();
  
        // Obtient une référence de type A
        A ref;
          
        // ref réfère à un objet A
        ref = a;
  
        // Appel de la méthode m1() de l'objet A
        ref.m1();
  
        // maintenant ref réfère à un objet B
        ref = b;
  
        // Appel de la méthode m1() de l'objet B
        ref.m1();
  
        // maintenant ref réfère à un objet C
        ref = c;
  
        // Appel de la méthode m1() de l'objet C
        ref.m1();
    }
}

Sortie: 
Dans la méthode m1 de la classe A
Dans la méthode m1 de la classe B
Dans la méthode m1 de la classe C
```


## Super propriété

_super_ permet d'appeler la propriété ou la variable d'un parent.

```java
class Animal{  
  String couleur="blanc";  
}  

class Chien extends Animal{  
  String couleur="noir";  
  void imprimeCouleur(){  
    System.out.println(couleur);        // affiche la couleur de la classe Chien 
    System.out.println(super.couleur);  // affiche la couleur de la classe Animal
  }  
}

class Super1{  
  public static void main(String args[]){  
    Chien c=new Chien();  
    c.imprimeCouleur();  
    // affiche noir et blanc
  }
}  
```

## Super méthode

La méthode _super_ permet à une méthode enfant d'appeler la méthode du parent:

```java
class Animal{  
  void mange(){
    System.out.println("mange...");
  }  
}  

class Chien extends Animal{  
  void mange(){
    System.out.println("mange moulée...");
  }  
  
  void aboie(){
    System.out.println("jappe...");
  }    
  
  void fonctionne(){  
    super.mange();  
    aboie();  
  }  
} 

class Super2{  
  public static void main(String args[]){  
    Chien c=new Chien();  
    c.fonctionne();  
  }
}  
```

## Super constructeur

_this_ permet de spécifier la classe en cours.

```java
class Personne{  
  int id;  
  String nom;  
  
  Personne(int id, String nom){  
    this.id=id;  
    this.nom=nom;  
  }  
}

class Employe extends Personne{  
  float salaire;
  
  Employe(int id, String nom, float salaire){  
    super(id,nom);      //réutilisation du constructeur parent 
    this.salaire=salaire;  
  }  
  
  void affiche(){
    System.out.println(id+" "+nom+" "+salaire);
  }  
}  

class Super5{  
  public static void main(String[] args){  
    Employe e1=new Employe(1,"Michel", 45000);  
    e1.affiche();  
  }
}  
```

# Abstract

Une classe abstraite ne peut pas être instanciée. Dans l'exemple précédent si Animal était déclarée _abstract_, elle ne pourrait pas être instanciée à l'appel suivant:

```java
abstract class Animal {
  public void animalSon() {
    System.out.println("Les animaux font un son");
  }
}
... 

Animal monAnimal = new Animal();
```

![](images/abstract.png)

Une méthode abstraite peut être définie dans une classe abstraite, elle n'aura aucun corps dans la classe abstraite, seulement sa déclaration. Elle forcera ainsi les classes enfants à définir leur méthode:

```java
abstract class Animal {
  public abstract void animalSon();
}
... 

Animal monAnimal = new Animal();  // Crée un objet animal
```



# Interface

Définition : 

* Définit un comportement qui doit être implémenté par une classe: ensemble de méthodes (signatures uniquement) que doit avoir une classe pour respecter l'interface
* Une interface n’implémente aucun objet, aucune méthode. Elle indique seulement son modèle
* On dit qu’une interface est un contrat qui doit être respecté par les développeurs
* C'est comme une classe uniquement abstraite qui impose une structure aux classes

```java
// interface
interface Animal {
  public void animalSon();
  public void dormir(); 
}

class Cochon implements Animal {
  public void animalSon() {    
    System.out.println("Le cochon fait: groin groin");
  }
  public void dormir() {    
    System.out.println("zzz");
  }
}

class Main {
  public static void main(String[] args) {
    Cochon monCochon = new Cochon();
    monCochon.animalSon();
    monCochon.dormir();
  }
}
```

### Notes sur les interfaces

* Ne peut être utilisé pour créer des objets
* Les méthodes d'interfaces n'ont pas de corps, elles sont définies par la classe implémentant l'interface
* Vous ne pouvez implémenter qu'une partie des méthodes, vous devez implémenter TOUTES les méthodes pour correspondre à l'interface.
* Les méthodes des interfaces sont abstraites et publiques par défaut.
* Il n'y a pas de variables dans une interface, les propriétés ainsi définies sont publiques, statiques et finales.
* Une interface ne peut contenir un constructeur (puisque l'interface ne peut créer des objets) 

### Quand utiliser une interface ?

* Java ne supporte pas les héritages multiples "multiple inheritance". Par contre, on peut utiliser les interfaces car les classes peuvent implémenter plusieurs interfaces en les séparant avec des virgules.

## _Interface default_

Java 8 a introduit les _Default Method_ (ou _defender methods_) à l'intérieur des interfaces. Cela permet d'ajouter de nouvelles méthodes à une interface existante sans briser les implémentations actuelles. Cela permet d'ajouter une méthode qui servira d'implémentation dans une situation où une classe concrète ne répond pas à un cas d'utilisation d'une méthode d'interface.

```java
public interface nouvelleInterface {
    public void methodeExistante();
    default public void nouvelleMethodeParDefaut() {
        System.out.println("Nouvelle méthode par défaut"
              " est ajoutée à l'interface");
    }
}
```

# _Collection Interface_

![](images/Collection.png)

### _Interface List_ 

Modéliser une collection ordonnée qui accepte les doublons.

### _Interface Set_ 

Modéliser une collection non ordonnée qui n'accepte pas les doublons.

### _Interface Map_ 

Modéliser une collection sous la forme clé/valeur (_key/value_), non ordonnée qui n'accepte pas les doublons.

## Méthodes de l'interface Collection
```java
boolean add(Element e)
```
* Ajoute un élément à la collection

```java
boolean addAll(Collection c)
```
* Ajoute tous les éléments d'une collection à la collection

```java
boolean remove(Object o)
```
* Supprime un élément de la collection s'il est présent

```java
boolean removeAll(Collection c)
```
* Supprime tous les éléments d'une collection s'ils sont présents 

```java
boolean retainAll(Collection c)
```
* Conserve que les éléments communs d'une collection les autres sont supprimés. Le boolean retourné indique si la collection a été modifiée.

```java
boolean contains(Object o)
```
* Indique si l'objet est présent dans la collection

```java
boolean containsAll(Collection o)
```
* Indique si les éléments d'une collection sont présents dans la collection

```java
void clear()
```
* Supprime tous les éléments de la collection

```java
boolean isEmpty()** 
```
* Indique si la collection est vide

```java
int size()**
```
* Retourne le nombre d'éléments de la collection

```java
Object[] toArray()**
```
* Retourne un tableau des éléments de la collection

# Interface _Marker_

Les interfaces _Marker_ sont des façons d'informer le compilateur que la classe va avoir un comportement particulier lorsque nous allons l'instancier. 

Les interfaces _Marker_ n'ont **aucune** méthode.

Les interfaces _Marker_ se font nommer _Tagging interface_.

Voici un exemple de _Marker_ personnalisé:

```java
public interface Deletable {
}

public class Entity implements Deletable {
    // implementation details
}

public class ShapeDao {

    public boolean delete(Object object) {
        if (!(object instanceof Deletable)) {
            return false;
        }

        // Efface l'objet

        return true;
    }
}
```

## _Cloneable_ (https://docs.oracle.com/javase/7/docs/api/java/lang/Cloneable.html)

Permet à une classe d'être _clonable_, c'est à dire de répondre à l'appel à la méthode clone(). Les objets doivent implémenter _Cloneable_ afin que l'on puisse appeler clone() sur eux. En gros, ça rend légal la méthode de copier chaque propriétés de la classe pour chaque instance de la classe.

Il est généralement recommandé d'_overider_ la méthode clone de Object afin d'obtenir un objet de la classe qu'on veut rendre _cloneable_.

```java
class Student { // implements Cloneable {

    // attributes of Student class
    String name = null;
    int id = 0;

    // default constructor
    Student() {}

    // parameterized constructor
    Student(String name, int id)
    {
        this.name = name;
        this.id = id;
    }

    @Override
    protected Student clone()
            throws CloneNotSupportedException
    {
        return (Student) super.clone();
    }

    @Override
    public String toString(){
        return String.format("%s (%s)", this.name, this.id);
    }
}

public static void main(String[] args) {
    Student s1 = new Student("Ashish", 121);
    Student s2 = new Student();

    // Try to clone s1 and assign
    // the new object to s2
    try {
        s2 = s1.clone();

...
```

## Serialization (https://docs.oracle.com/javase/8/docs/api/java/io/Serializable.html)

Ce _marker_ nous permet de transférer aisément un objet en _Stream_ et la désérialization va permettre de faire l'inverse. L'opération complète est indépendante de la plateforme, c'est à dire, que vous pourriez prendre un objet sérialisé en Linux et le désérialiser sur Windows. 

Quand on dit qu'on sérialise un objet, on fait appel à la méthode suivante provenant d'un objet de type _ObjectOutputStream_:

```java
public final void writeObect (Object obj) throws IOException
```

Et quand on le désérialise, on fait appel à la méthode suivante provenant de la classe _ObjectInputStream_:

```java
public final Object readObject () throws IOException, ClassNotFoundException
```

Java nous offre une interface Serializable afin d'identifier les classes et les propriétés qui doivent être persistentes, i.e. les données sont accessibles lors de l'exécution.

Tous les éléments de la classes doivent être identifiés comme sérialisables si on veut pouvoir sérialiser une classe. Par exemple, dans l'exemple ci-dessous, si un _Student_ possédait une adresse n'étant pas Sérializable, _Student_ ne pourra plus être sérialisé.

```java
class Student implements Serializable {

    // attributedis of Student class
    String name = null;
    int id = 0;

    // default constructor
    Student() {}

    // parameterized constructor
    Student(String name, int id)
    {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString(){
        return String.format("%s (%s)", this.name, this.id);
    }
}

public static void main(String[] args)  {
    Student s1 = new Student("Ashish", 121);
    FileOutputStream outputStream;

    try
    {
        outputStream = new FileOutputStream ("destination.txt");
        ObjectOutputStream out=new ObjectOutputStream(outputStream);
        out.writeObject(s1);
        out.flush();
        outputStream.close();
    } catch (IOException e) {
        System.out.println("Fichier non trouvé");
    }

    FileInputStream inputStream;
    Student s2 = new Student();

    try
    {
        inputStream = new FileInputStream ("destination.txt");
        ObjectInputStream in=new ObjectInputStream(inputStream);
        s2 = (Student) in.readObject();
        inputStream.close();
    } catch (IOException e) {
        System.out.println("Fichier non trouvé");
    } catch ( ClassNotFoundException e) {
        System.out.println("Structure du fichier non approprié");
    }

    System.out.println(s2);
}
```

### Transient

Lorsque nous notons un champs transient, la donnée n'est pas sérialisée:

```java
public class Book implements Serializable {
    private String bookName;
    private transient String description;
    private transient int copies;
    
    // getters and setters
}

```

Ainsi dans le _main_, les données descriptions et copies resteront à null et à 0 lorsque lus du fichier.

## _Remote_ (https://docs.oracle.com/javase/8/docs/api/java/rmi/Remote.html)

Un objet _remote_ est un objet conservé sur une machine et accédé par une autre machine. Pour rendre un objet _remote_, il faut lui assigner le _flag_ d'interface _Remote_. Ainsi, le _remote interface_ permet d'identifier les méthodes qui peuvent être exécutées par une machine virtuelle JVM non locale.

## _RandomAccess_ (https://docs.oracle.com/javase/8/docs/api/java/util/RandomAccess.html)

Une interface de marquage qui permet d'indiquer que l'interface supporte un accès rapide (constant) d'accès aléatoire.

# Retour sur les collections

## Quel est le lien entre _ArrayList_, _Collection_, _List_, _AbstractList_ et _AbstractCollection_?

* _ArrayList_, _AbstractList_ et _AbstractCollection_ sont des classes, elles peuvent donc être implémentées.
* _Collection_ et _List_ sont des Interfaces, elles contiennent des listes de méthodes devant être implémentées pour correspondre à ces interfaces. 
* _AbstractList_ découle de _AbstractList_ qui découle de _AbstractCollection_.

N'oubliez jamais que les objets en Java ne peuvent découler que d'une seule et unique classe, par contre, ils peuvent répondre à une multitude d'interface.

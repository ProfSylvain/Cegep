# _Array_ vs _ArrayList_

Il y a deux éléments (en réalité, plus) permettant de gérer les ensembles d'éléments. Lors du cours Prog 1, vous avez joué principalement avec les _arrays_. 

## _Array_

Un _array_ est un objet créé dynamiquement, il contient un nombre constant d'éléments de même type. La mémoire allouée pour l'objet est un emplacement en mémoire centrale contiguë. Lorsqu'un _array_ est créé, on ne peut changer sa taille, on le crée en utilisant la méthode suivante:

```java
int arr[]=new int[4]; 

arr[0]=12;  
arr[1]=2;  
arr[2]=15;  
arr[3]=67;  
```

## _ArrayList_ 

Puisque Java est orienté objet, il est important d'avoir un objet permettant de mieux gérer les tableaux que le _Array_ de base, les développeurs de Java ont donc créé le _ArrayList_. Un _ArrayList_ offre un certain nombre de méthodes prédéfinies:

* add()
* addAll()
* contains()
* clear()
* get()
* isEmpty()
* indexOf()
* remove()
* set()
* size()
* toArray()
* ...

https://docs.oracle.com/javase/8/docs/api/java/util/List.html

Une interface est un type d'objet, en programmation orientée objet, qui oblige un élément à avoir un certain nombre de méthodes prédéfinies lui permettant ainsi de répondre à la définition de _List_.

Donc un _ArrayList_ a accès à la méthode _add_ qui lui permet donc d'ajouter des éléments aisément.

Le _ArrayList_ est créé dynamiquement mais peut contenir un nombre variable d'éléments.

```java
ArrayList<Type> arrayList=new ArrayList<Type>();  

ArrayList <Integer> list=new ArrayList<Integer>();  
list.add(12);   
```

Il va en réalité effectuer ceci:

```java
list.add(new Integer(12)); 
```

## Transtypage liste vers tableau

### Exemple 1

```java
List<String> list = new ArrayList<String>();

list.add("Michel");
list.add("Philippe");
list.add("Charles");
list.add("Sylvain");

String[] arr = new String[list.size()];

for (int i = 0; i < list.size(); i++)
    arr[i] = list.get(i);

for (String x : arr)
    System.out.print(x + " ");
```

### Exemple 2
```java
List<String> list = new ArrayList<String>();

list.add("Michel");
list.add("Philippe");
list.add("Charles");
list.add("Sylvain");

String[] array = new String[list.size()];
array = list.toArray(array);

bh.consume(array);
```


## Transtypage tableau vers liste

### Exemple 1
```java
String[] array = new String[]{"Michel", "Philippe", "Charles", "Sylvain"};

ArrayList<String> array_list =
        new ArrayList<String>();

for (int i = 0; i < array.length; i++)
    array_list.add(array[i]);

bh.consume(array);
bh.consume(array_list);
```

### Exemple 2

```java
String[] array = new String[]{"Michel", "Philippe", "Charles", "Sylvain"};
ArrayList<String> array_list = new ArrayList<String>(Arrays.asList(array));

bh.consume(array);
bh.consume(array_list);
```

### Exemple 3
```java

String[] array = new String[]{"Michel", "Philippe", "Charles", "Sylvain"};

ArrayList<String> array_list = new ArrayList<String>();

Collections.addAll(array_list, array);

bh.consume(array);
bh.consume(array_list);
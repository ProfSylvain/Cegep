# Dansons avec le quicksort

https://www.youtube.com/watch?v=3San3uKKHgg

## Fonctionnement

Trois étapes importantes

### Pivot

On choisit un pivot, c'est à dire un élément sur lequel on va trier les éléments (plus grand ou plus petit que le pivot).

### Swap 

On échange deux éléments positionnés à index1 et index2.

### Partionnement

Nommons deux pointeurs:

* lp qui sert à parcourir la liste à partir de la gauche
* rp qui sert à parcourir la liste à partir de la droite

On parcourt la liste à partir de la gauche jusqu'à ce qu'on trouve un élément plus grand que le pivot. 

Si on en trouve un, on parcourt la liste à partir de la droite jusqu'à ce qu'on trouve un élément plus petit que le pivot. 

Si lp < rp, on swap les deux éléments.

Si lp = rp, on swap le pivot avec l'élément à LP.

L'élément désormais à la position LP est à la bonne place. On refait l'algorithme avec deux nouveaux tableaux: la gauche de LP et la droite de LP.

## Exemple image

![](img/Exemple.png)


## Explication

Soit un array

19, 23, 34, 45, 31, 22, 10, 13, 25, 37

Nous allons identifier un pivot, une valeur avec laquelle on va séparer, le choix du pivot est important mais pour l'instant, nous allons utiliser le dernier élément pour simplifier le fonctionnement: 37.

19, 23, 34, 45, 31, 22, 10, 13, 25, **37**

Nous allons créer deux pointeurs : left et right.

left part de la gauche et se déplace vers la droite jusqu'à temps qu'il découvre un chiffre plus grand que le pivot: 37.

right part de la droite et se déplace vers la gauche jusqu'à temps qu'il découvre un chiffre plus petit que le pivot 37.

On part du côté gauche:
```
lp = 0: 19 <= 37 Vrai
lp = 1: 23 <= 37 Vrai
lp = 2: 34 <= 37 Vrai
lp = 3: 45 <= 37 Faux!!!
```

On part désormais du côté droit:
```
rp = 9: 37 >= 37 Vrai
rp = 8: 25 >= 37 Faux
```

On swap les données, l'array devient le suivant:

19, 23, 34, 25, 31, 22, 10, 13, 45, **37**

On continue à augmenter la gauche
```
lp = 4: 31 <= 37 Vrai
lp = 5: 22 <= 37 Vrai
lp = 6: 10 <= 37 Vrai
lp = 7: 13 <= 37 Vrai
lp = 8: 45 <= 37 Faux
```

Lorsque lp = rp, on échange le pivot avec le LP et on relance de chaque côté de la position.

19, 23, 34, 25, 31, 22, 10, 13, **37**, 45

En gros on sait que le 37 est désormais bien placé. On peut trier à gauche et à droite.

[19, 23, 34, 25, 31, 22, 10, 13]

et

[45]

Puisque le 45 est seul, il est déjà trié. On s'occupe alors seulement du côté gauche, ne pas oublier que le 37 est déjà trié.

19, 23, 34, 25, 31, 22, 10, **13**

Le pivot devient le 13

```
lp = 0: 19 <= 13 Faux
```

On arrête

```
rp = 7: 13 >= 13 Vrai
rp = 6: 10 >= 13 Faux
```

On swap

10, 23, 34, 25, 31, 22, 19, **13**

Tous les autres éléments du tableau sont désormais plus grand que 13, donc 

```
lp = 1: 23 <= 13 Faux
```

Cependant rp va se rendre jusqu'à 1 puisque 22, 31, 25 et 34 sont plus grand que 13.

```
rp = 5: 22 >= 13 Vrai
rp = 4: 22 >= 31 Vrai
rp = 3: 22 >= 25 Vrai
rp = 2: 22 >= 34 Vrai
rp = 1: 22 >= 23 Vrai
```

Cependant lp = rp

On swap le pivot avec lp.

10, 13, 34, 25, 31, 22, 19, 23

On relance de nouveau l'algorithme avec les deux nouveaux sous-tableaux:

```
[10] (déjà trié)
[34, 25, 31, 22, 19, **23**]

lp va arrêter à 34
rp va arrêter à 19
```

swap

```
19, 25, 31, 22, 34, **23**

lp va arrêter à 25
rp va arrêter à 22
```

swap

```
19, 22, 31, 25, 34, **23**

lp va arrêter à 31
rp va arrêter à 31
```

swap avec le pivot

19, 22, **23**, 25, 34, 31

On sait dorénavant que le 23 est bien situé.

[19, **22**] 

[25, 34, **31**]

```
lp va arrêter à 34
rp va arrêter à 34
```

On swap le pivot et lp

[25, 31, 34 ]

Ce qu'il faut comprendre c'est que le tout s'est fait sur  le même tableau, on a juste joué avec des index.

Voyons le code en vrai et l'exécution en code.

## Optimisation

Choisir le pivot est une étape critique, une bonne façon est d'utiliser un _random_ dans l'_array_ réduit et ne pas toujours sélectionner le dernier.
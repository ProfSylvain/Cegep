# Récursivité

Concept utilisé dans tous les langages de programmation, qui consiste à écrire une méthode qui s'appelle elle-même. Ça ne répond pas à toutes les solutions algorithmiques.

Utilisation de la récursivité:

* cas mathématiques
* algorithmes de tri
* algorithmes de fouille

Exemple:

La factorielle d'un entier n est le produit des nombres entiers positifs inférieurs ou égaux à n.

### Factorielle itératif

```java
0! = 1
1! = 1
2! = 2 * 1 = 2
3! = 3 * 2 * 1 = 6
4! = 4 * 3 * 2 * 1 = 24
10! = 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1 = 3 628 800

public static long factorielle(long n){
    long accumulateur = 1;

    for (long i = n; i >= 1; i--)
        accumulateur *= i;

    return accumulateur;
}
```

### Factorielle récursif

Structure fonction récursive:

- Cas d'arrêt
- Cas récursif

```java
0! = 1 = 1
1! = 1 * 0! = 1
2! = 2 * 1! = 2
3! = 3 * 2! = 6
4! = 4 * 3! = 24

n! signifie n * (n - 1)!

public static long factorielleR(long n){
    if (n == 0)                          //cas d'arrêt
        return 1;
    else                                 //cas récursif
        return n * factorielleR(n - 1);
}
```

Trace:

![](img/recursivite.jpeg)
# Rappel fouille séquentielle

N'exige pas que les éléments du tableau soient ordonnés.
* Retourne la position de la première occurrence s'il y a plusieurs éléments de la même valeur dans le tableau.
* Dans le pire des cas (si la valeur recherchée n'est pas dans le tableau) le nombre d'éléments consultés sera égal au nombre d'éléments dans le tableau.
* Principe: tant que la valeur recherchée n'est pas trouvée et que l'indice du tableau n'est pas à la fin -> avance au prochain élément.

![](img/S%C3%A9quentielle.png)

# Fouille dichotomique

* Exige que les éléments du tableau soient ordonnés.
* Le principe: couper en deux l'espace de fouille
  1. Déterminer si l'élément est au milieu du tableau, sinon:
    * si l'élément est plus petit que le milieu fouiller la partie gauche;
    * si l'élément est plus grand que le milieu fouiller la partie droite;
  2. On arrête lorsqu'on a trouvé ou lorsqu'on ne peut plus couper en deux l'espace de fouille.

## Algorithme
```java
public static int fouilleDichotomique(int[] tableau, int valeur){
    int debut = 0;
    int fin = tableau.length - 1;
    int milieu = 0;
    boolean trouve = false;
    
    while (debut <= fin && !trouve){
        milieu = (debut + fin) / 2;
        
        if (valeur == tableau[milieu])
            trouve = true;
        else if (valeur < tableau[milieu])
            fin = milieu - 1;
        else
            debut = milieu + 1;
    }

    if (trouve)
        return milieu;
    else
        return -1;
}
```

## Comparaison
* Fouille séquentielle O(n)
* Fouille dichotomique O(log n)

Pour un tableau de 512 éléments: si position à l'indice 510
  *  fouille séquentielle -> 510 itérations
  *  fouille dichotomique -> 10 itérations

![](img/O.png)

# Analyse de la complexité d'un algorithme

Source: Wikipédia

L'analyse de la complexité d'un algorithme consiste en l'étude formelle de la quantité de ressources (par exemple : temps ou espace) nécessaire à l'exécution de cet algorithme. 

Celle-ci ne doit pas être confondue avec la théorie de la complexité, qui elle étudie la difficulté intrinsèque des problèmes, et ne se focalise pas sur un algorithme en particulier.

## Exemple

Supposons que le problème posé soit; trouver un nom dans un annuaire téléphonique selon une liste triée alphabétiquement. On peut s'y prendre de plusieurs façons différentes. En voici deux :

* **Recherche linéaire** : parcourir les pages dans l'ordre (alphabétique) jusqu'à trouver le nom cherché.
* **Recherche dichotomique** : ouvrir l'annuaire au milieu, si le nom qui s'y trouve est plus loin alphabétiquement que le nom cherché, regarder avant, sinon, regarder après. Refaire l'opération qui consiste à couper les demi-annuaires (puis les quarts d'annuaires, puis les huitièmes d'annuaires, etc.) jusqu'à trouver le nom cherché.

![](img/Dichotomique.png)

Pour chacune de ces méthodes il existe un pire des cas et un meilleur des cas. Prenons la méthode 1 :

* Le meilleur des cas est celui où le nom est le premier dans l'annuaire, le nom est alors trouvé instantanément.
* Le pire des cas est celui où le nom est le dernier dans l'annuaire, le nom est alors trouvé après avoir parcouru tous les noms.

Si l'annuaire contient 30 000 noms, le pire cas demandera 30 000 étapes. La complexité dans le pire des cas de cette première méthode pour n entrées dans l'annuaire fourni est O(n), ça veut dire que dans le pire des cas, le temps de calcul est de l'ordre de grandeur de n: il faut parcourir tous les n noms une fois.

Le second algorithme demandera dans le pire des cas de séparer en deux l'annuaire, puis de séparer à nouveau cette sous-partie en deux, ainsi de suite jusqu'à n'avoir qu'un seul nom. 

Le nombre d'étapes nécessaire sera le nombre entier qui est immédiatement plus grand que 
log<sub>2</sub>n qui, quand n est 30 000, est 15 (car 2<sup>15</sup> vaut 32 768). La complexité (le nombre d'opérations) de ce second algorithme dans le pire des cas est alors O(log<sub>2</sub>n), ce qui veut dire que l'ordre de grandeur du nombre d'opérations de ce pire cas est le logarithme en base 2 de la taille de l'annuaire, c'est-à-dire que pour un annuaire dont la taille est comprise entre 
2<sup>p-1</sup> et 2<sup>p</sup>, il sera de l'ordre de p. On peut écrire aussi bien O(ln n)ou O(log<sub>2</sub>n) car ln et log<sub>2</sub>n ont le même ordre de grandeur.𝑂


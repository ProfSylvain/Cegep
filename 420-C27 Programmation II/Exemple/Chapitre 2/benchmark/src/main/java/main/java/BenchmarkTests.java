package main.java;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.infra.Blackhole;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static main.java.ArrayVSArrayList.addItemToArray;
import static main.java.ArrayVSArrayList.addItemToArrayList;


public class BenchmarkTests {

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchAddItemNewArray(Blackhole bh) {
        int[] array = new int[0];
        Random r = new Random();

        for (int i = 0; i < 100000; i++) {
            int x = r.nextInt(Integer.MAX_VALUE);
            array = addItemToArray(array, x );
        }
        bh.consume(array);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchAddItemSameArray(Blackhole bh) {
        int[] array = new int[100000];
        Random r = new Random();

        for (int i = 0; i < 100000; i++) {
            array[i] = r.nextInt(Integer.MAX_VALUE);
        }
        bh.consume(array);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchAddItemArrayList(Blackhole bh) {
        ArrayList<Integer> array = new ArrayList<>(100000);
        Random r = new Random();

        for (int i = 0; i < 100000; i++) {
            int x = r.nextInt(Integer.MAX_VALUE);
            addItemToArrayList(array, x );
        }
        bh.consume(array);
    }


    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchAddItemAndSortNewArray(Blackhole bh) {
        int[] array = new int[0];
        Random r = new Random();

        for (int i = 0; i < 100000; i++) {
            int x = r.nextInt(Integer.MAX_VALUE);
            array = addItemToArray(array, x );
        }

        Arrays.sort(array);
        bh.consume(array);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchAddItemAndSortSameArray(Blackhole bh) {
        int[] array = new int[100000];
        Random r = new Random();

        for (int i = 0; i < 100000; i++) {
            array[i] = r.nextInt(Integer.MAX_VALUE);
        }

        Arrays.sort(array);
        bh.consume(array);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchAddItemAndSortArrayList(Blackhole bh) {
        ArrayList<Integer> array = new ArrayList<>(100000);
        Random r = new Random();

        for (int i = 0; i < 100000; i++) {
            int x = r.nextInt(Integer.MAX_VALUE);
            addItemToArrayList(array, x );
        }

        Collections.sort(array);
        bh.consume(array);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchListToArray1(Blackhole bh){
        List<String> list = new ArrayList<String>();

        list.add("Michel");
        list.add("Philippe");
        list.add("Charles");
        list.add("Sylvain");

        String[] array = new String[list.size()];

        for (int i = 0; i < list.size(); i++)
            array[i] = list.get(i);

        bh.consume(array);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchListToArray2(Blackhole bh){
        List<String> list = new ArrayList<String>();

        list.add("Michel");
        list.add("Philippe");
        list.add("Charles");
        list.add("Sylvain");

        String[] array = new String[list.size()];
        array = list.toArray(array);

        bh.consume(array);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchArrayToList1(Blackhole bh) {

        String[] array = new String[]{"Michel", "Philippe", "Charles", "Sylvain"};

        ArrayList<String> array_list =
                new ArrayList<String>();

        for (int i = 0; i < array.length; i++)
            array_list.add(array[i]);

        bh.consume(array);
        bh.consume(array_list);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchArrayToList2(Blackhole bh) {
        String[] array = new String[]{"Michel", "Philippe", "Charles", "Sylvain"};
        ArrayList<String> array_list = new ArrayList<String>(Arrays.asList(array));

        bh.consume(array);
        bh.consume(array_list);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Benchmark
    public static void benchArrayToList3(Blackhole bh) {

        String[] array = new String[]{"Michel", "Philippe", "Charles", "Sylvain"};

        ArrayList<String> array_list = new ArrayList<String>();

        Collections.addAll(array_list, array);

        bh.consume(array);
        bh.consume(array_list);

    }
}


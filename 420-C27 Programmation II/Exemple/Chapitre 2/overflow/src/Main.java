public class Main {
    public static void overflow(int i){
        System.out.println(String.format("%d", i));
        overflow(++i);
    }
    public static void main(String[] args) {

        int i = 0;
        try {
            overflow(i);
        }catch (StackOverflowError e){
            System.out.println("J'ai tu fait exprès ou quoi?");
        }
    }
}
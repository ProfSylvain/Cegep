public class Main {

    public static long factorielle(long n){
        long accumulateur = 1;

        for (long i = n; i >= 1; i--)
            accumulateur *= i;

        return accumulateur;
    }

    public static long factorielleR(long n){
        if (n == 0)                          //cas d'arrêt
            return 1;
        else                                 //cas récursif
            return n * factorielleR(n - 1);
    }



    public static void main(String[] args) {
        System.out.println(factorielleR(5));
    }
}
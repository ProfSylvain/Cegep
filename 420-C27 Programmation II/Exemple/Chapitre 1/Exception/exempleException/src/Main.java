import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static int somme(int a, int b) throws Exception {

        if (b > 0  && a > Integer.MAX_VALUE - b)
            throw new Exception("Dépassement de capacité");
        else if (b < 0 && a < Integer.MIN_VALUE - b)
            throw new Exception("Dépassement de capacité");
        else
            return a+b;

    }
    public static int age1() {
        Scanner inputScanner = new Scanner(System.in);
        int age = 0;

        boolean endLoop = true;
        while (endLoop) {
            try {
                System.out.println("Quel est vôtre age?");
                age = inputScanner.nextInt();
                endLoop = false;
            } catch (InputMismatchException e) {
                System.out.println("Veuillez entrez un nombre");
                inputScanner.nextLine();
            }
        }

        return age;
    }

    public static int age2(){
        Scanner inputScanner = new Scanner(System.in);
        int age = 0;

        boolean endLoop = true;
        while (endLoop) {

            System.out.println("Quel est vôtre age?");
            if (inputScanner.hasNextInt()) {
                age = inputScanner.nextInt();
                endLoop = false;
            } else {
                System.out.println("Veuillez entrez un nombre");
                inputScanner.nextLine();
            }
        }

        return age;
    }

    public static void main(String[] args) {
        int age;

        age = age1();
        //age = age2();

        System.out.format("Vous avez %d ans", age);
    }
}
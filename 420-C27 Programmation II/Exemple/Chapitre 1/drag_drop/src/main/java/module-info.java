module com.example.drag_drop {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.drag_drop to javafx.fxml;
    exports com.example.drag_drop;
}
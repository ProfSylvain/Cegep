module com.example.eventhandler2 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.eventhandler2 to javafx.fxml;
    exports com.example.eventhandler2;
}
module com.example.changelistener {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.changelistener to javafx.fxml;
    exports com.example.changelistener;
}
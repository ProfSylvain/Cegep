import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Sylvain Rivard
 */
public class Main {

    static String[][] tabBieres = new String[5][4];
    static int nbBieres;

    public static void main(String[] args) throws IOException {
        exercice1();
        exercice2();
    }

    /**
     * Méthodes pour l’exercice 1
     */

    private static void exercice1() throws IOException {
        Scanner scan = new Scanner(System.in);
        int debut, fin;

        System.out.print("Entrer le nombre de début et le nombre de fin: ");
        debut = scan.nextInt();
        fin = scan.nextInt();

        ecrireFichierExercice1("texte/nombres.txt", debut, fin);
        lireFichierExercice1("texte/nombres.txt");
    }

    public static void ecrireFichierExercice1(String fileName, int n1, int n2) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false));
        for (int i=n1; i<=n2; i++){
            writer.write(String.valueOf(i));
            writer.newLine();
        }
        writer.close();
    }

    public static void lireFichierExercice1(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String ligne;
        while ((ligne = reader.readLine()) != null)
            System.out.println(ligne);
        reader.close();
    }

    /**
     * Méthode pour l’exercice 2
     */

    private static void exercice2() throws IOException {
        lireFichierExercice2("texte/bieres.txt");
        for (int i=0; i<nbBieres; i++)
            System.out.println(Arrays.toString(tabBieres[i]));
    }

    private static void lireFichierExercice2(String fileName) throws IOException {
        String[] tab;
        String ligne;
        nbBieres = 0;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));

            while ((ligne = reader.readLine()) != null) {
                tab = ligne.split("/");

                tabBieres[nbBieres][0] = tab[0];
                tabBieres[nbBieres][1] = tab[1];
                tabBieres[nbBieres][2] = tab[2];
                tabBieres[nbBieres][3] = tab[3];

                // instruction équivalente
                // tabBieres[nbBieres] = tab;

                nbBieres++;
            }

            reader.close();
        } catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        }

    }
}

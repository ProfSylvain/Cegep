import java.io.*;

class Student implements Serializable {

    // attributedis of Student class
    String name = null;
    int id = 0;

    // default constructor
    Student() {}

    // parameterized constructor
    Student(String name, int id)
    {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString(){
        return String.format("%s (%s)", this.name, this.id);
    }
}

public class Main {
    public static void main(String[] args)  {
        Student s1 = new Student("Ashish", 121);
        FileOutputStream outputStream;

        try
        {
            outputStream = new FileOutputStream ("destination.txt");
            ObjectOutputStream out=new ObjectOutputStream(outputStream);
            out.writeObject(s1);
            out.flush();
            outputStream.close();
        } catch (IOException e) {
            System.out.println("Fichier non trouvé");
        }

        FileInputStream inputStream;
        Student s2 = new Student();

        try
        {
            inputStream = new FileInputStream ("destination.txt");
            ObjectInputStream in=new ObjectInputStream(inputStream);
            s2 = (Student) in.readObject();
            inputStream.close();
        } catch (IOException e) {
            System.out.println("Fichier non trouvé");
        } catch ( ClassNotFoundException e) {
            System.out.println("Structure du fichier non approprié");
        }

        System.out.println(s2);
    }
}
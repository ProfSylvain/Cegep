class Animal {
    public void animalSon() {
        System.out.println("Les animaux font un son");
    }
}

class Cochon extends Animal {
    @Override
    public void animalSon() {
        System.out.println("Le cochon fait: groin groin");
    }
}

class Chien extends Animal {
    @Override
    public void animalSon() {
        System.out.println("Le chien fait: wouf wouf");
    }
}

class Main {
    public static void main(String[] args) {
        Animal monAnimal = new Animal();        // Crée un objet Animal
        Animal monCochon = new Cochon(); // Crée un objet Cochon
        Animal monChien = new Chien();   // Crée un objet Chien
        monAnimal.animalSon();
        monCochon.animalSon();
        monChien.animalSon();
    }
}
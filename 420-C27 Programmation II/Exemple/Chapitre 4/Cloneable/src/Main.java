
class Student { // implements Cloneable {

    // attributedis of Student class
    String name = null;
    int id = 0;

    // default constructor
    Student() {}

    // parameterized constructor
    Student(String name, int id)
    {
        this.name = name;
        this.id = id;
    }

    @Override
    protected Student clone()
            throws CloneNotSupportedException
    {
        return (Student) super.clone();
    }

    @Override
    public String toString(){
        return String.format("%s (%s)", this.name, this.id);
    }
}

public class Main {
    public static void main(String[] args) {
        Student s1 = new Student("Ashish", 121);
        Student s2 = new Student();

        // Try to clone s1 and assign
        // the new object to s2
        try {
            s2 = s1.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("Cannot clone s1");
        }

        s2.name = "Jean";


        System.out.println(s1);
        System.out.println(s2);
    }
}
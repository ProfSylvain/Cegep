module com.example.heritage2 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.heritage2 to javafx.fxml;
    exports com.example.heritage2;
}
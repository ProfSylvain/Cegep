import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Main {


    public static void lambda1() {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(9);
        numbers.add(8);
        numbers.add(1);
        numbers.forEach( (n) -> { System.out.println(n*n); } );
    }

    interface StringFunction {
        String run(String str);
    }

    public static void lambda2() {
        StringFunction exclaim = (s) -> s + "!";
        StringFunction ask = (s) -> s + "?";
        printFormatted("Hello", exclaim);
        printFormatted("Hello", ask);
    }

    public static void printFormatted(String str, StringFunction format) {
        String result = format.run(str);
        System.out.println(result);
    }



    // Function

    public static class AddThree implements Function<Long, Long> {
        @Override
        public Long apply(Long aLong) {
            return aLong + 3;
        }
    }

    // Compare

    public static class Employee {
        int id;
        String name;
        double salary;
        public Employee(int id, String name, double salary) {
            super();
            this.id = id;
            this.name = name;
            this.salary = salary;
        }
    }

    public static void compare() {
        List<Employee> list = new ArrayList<Employee>();

        // Adding employees
        list.add(new Employee(115, "Charles", 75000.00));
        list.add(new Employee(125, "Sylvain",50000.00));
        list.add(new Employee(135, "Michel", 60000.00));

        System.out.println("Tri de la liste selon le salaire");

        //list.sort(new Comparator<Employee>() {
        //    @Override
        //    public int compare(Employee e1, Employee e2) {
        //        return (int) Math.round(e1.salary - e2.salary);
        //    }
        //});

        // lambda
        list.sort((e1, e2) -> {
            return (int) Math.round(e1.salary - e2.salary);
        });

        for(Employee e : list) {
            System.out.println(e.id + " " + e.name + " " + e.salary);
        }
    }





    public static void function1(){
        Function<Long, Long> adder = new AddThree();
        Long result = adder.apply((long) 4);
        System.out.println("result = " + result);
    }

    public static void function2() {
        // Avec lambda
        Function<Long, Long> adder = (value) -> value + 3;
        Long resultLambda = adder.apply((long) 4);
        System.out.println("resultLambda = " + resultLambda);
    }

    // Predicate

    public static void predicate() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(9);
        numbers.add(8);
        numbers.add(1);
        Predicate<Integer> predicate = (value) -> value > 5;

        numbers.stream().filter(predicate).forEach((n) -> System.out.println(n));
        numbers.removeIf(predicate);

        numbers.forEach( (n) -> System.out.println("Après effacement: " +  n));
    }

    public static class Customer {
        private String name;
        private int points;

        public Customer(String name, int points) {
            this.name = name;
            this.points = points;
        }

        public int getPoints(){
            return this.points;
        }

        public String toString() {
            return this.name + " (" + this.points + ")";
        }
    }

    public static void stream() {

        Customer john = new Customer("John P.", 15);
        Customer sarah = new Customer("Sarah M.", 200);
        Customer charles = new Customer("Charles B.", 150);
        Customer mary = new Customer("Mary T.", 1);

        List<Customer> customers = Arrays.asList(john, sarah, charles, mary);

        List<Customer> customersWithMoreThan100Points = customers
                .stream()
                .filter(c -> c.getPoints() > 100)
                .toList();

        customersWithMoreThan100Points.forEach((c)-> System.out.println(c));
    }

    // Supplier
    public static void supplier(){
        Supplier<Double> randomValue = () -> {
            Random r = new Random();
            return r.nextDouble(50.0);
        };

        // Print the random value using get()
        System.out.println(randomValue.get());
        System.out.println(randomValue.get());
        System.out.println(randomValue.get());
    }

    // Consumer
    public static void consumer(){
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(5);
        numbers.add(9);
        numbers.add(8);
        numbers.add(1);
        Consumer<Integer> method = (n) -> { System.out.println(n); };
        numbers.forEach( method );
    }



    public static void main(String[] args) {
        System.out.println("Hello world!");
        lambda1();
        lambda2();

        compare();
        function1();
        function2();

        stream();

        predicate();
        supplier();
        consumer();
    }
}
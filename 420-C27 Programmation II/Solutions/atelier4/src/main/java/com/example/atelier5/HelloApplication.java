package com.example.atelier5;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {

    public void calculateSomme(TextField edt1, TextField edt2, Text txtSomme) {
        int nbr1 = 0, nbr2 = 0, somme = 0;

        try {
            nbr1 = Integer.parseInt(edt1.getText());
            nbr2 = Integer.parseInt(edt2.getText());

            // Dépassement de capacité
            if ( ( nbr2 > 0 && nbr1 < Integer.MAX_VALUE - nbr2) ||
                 ( nbr2 <= 0 && nbr1 > Integer.MIN_VALUE - nbr2) ) {
                somme = nbr1 + nbr2;
                txtSomme.setText(String.valueOf(somme));
            } else {
                txtSomme.setText("Dépassement de capacité");
            }

        }
        catch (NumberFormatException e){
            System.out.println("Problème de transtypage en entier.");
        }
    }

    @Override
    public void start(Stage stage) throws IOException {
        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);

        TextField edt1 = new TextField();
        edt1.setAlignment(Pos.CENTER);
        edt1.setText("0");
        edt1.setTooltip(new Tooltip("Premier nombre"));

        TextField edt2 = new TextField();
        edt2.setAlignment(Pos.CENTER);
        edt2.setText("0");
        edt2.setTooltip(new Tooltip("Deuxième nombre"));

        Text txtSomme = new Text();

        edt1.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (!keyEvent.getText().matches("\\d")){
                    keyEvent.consume();
                }
                else{
                    calculateSomme(edt1, edt2, txtSomme);
                }
            }
        });

        edt2.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                System.out.println(keyEvent.getCode());
                if (!keyEvent.getText().matches("\\d")){
                    keyEvent.consume();
                }
                else {
                    calculateSomme(edt1, edt2, txtSomme);
                }
            }
        });



        vbox.getChildren().addAll(edt1, edt2, txtSomme);

        Scene scene = new Scene(vbox, 320, 240);
        stage.setTitle("Calculatrice vbox!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
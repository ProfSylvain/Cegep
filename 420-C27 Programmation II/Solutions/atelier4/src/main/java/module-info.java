module com.example.atelier5 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.atelier5 to javafx.fxml;
    exports com.example.atelier5;
}
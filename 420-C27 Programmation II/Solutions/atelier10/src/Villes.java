public class Villes {

    private static String[] villes = {"Québec", "Toronto", "Joliette", "Gaspé"};

    public static void afficherTab(int[][] donnees) {
        for (int i = 0 ; i < donnees.length; i++ ) {
            System.out.print(villes[i] + "   \t");
            for (int data : donnees[i]) {
                System.out.print(data + " ");
            }
            System.out.print("\n");
        }
    }

    public static void afficherTotal(int[][] donnees) {
        for (int i = 0 ; i < donnees.length; i++ ) {

            int somme = 0;
            System.out.print(villes[i] + "   \t");
            for (int data : donnees[i]) {
                somme += data;
            }
            System.out.print(somme + "\n");
        }
    }

    public static void afficherMoyenne(int[][] donnees) {
        for (int i = 0 ; i < donnees.length; i++ ) {

            int somme = 0;
            System.out.print(villes[i] + "   \t");
            for (int data : donnees[i]) {
                somme += data;
            }
            System.out.println(String.format("%.2f", (double) somme / donnees[i].length));
        }
    }

    public static void afficherMax(int[][] donnees) {
        for (int i = 0 ; i < donnees.length; i++ ) {

            int max = donnees[0][0];
            System.out.print(villes[i] + "   \t");
            for (int data : donnees[i]) {
                if (data > max) max = data;
            }
            System.out.println(max);
        }
    }

    public static void afficherMin(int[][] donnees) {
        for (int i = 0 ; i < donnees.length; i++ ) {

            int min = donnees[0][0];
            System.out.print(villes[i] + "   \t");
            for (int data : donnees[i]) {
                if (data < min) min = data;
            }
            System.out.println(min);
        }
    }
}

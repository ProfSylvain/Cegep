import com.example.atelier11.Fouille;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FouilleTest {

    public static String genereChaine() {
        final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        String result = "";
        int index;

        for (int i = 0; i < 10; i++) {
            index = random.nextInt(ALPHA.length());
            result += ALPHA.charAt(index);
        }

        return result;
    }


    public static double dicho (String [] tab, String recherche, int expected){
        System.out.println("Recherche: " + recherche);

        long startTime = System.nanoTime();
        assertEquals(expected, Fouille.fouilleDichotomique(tab, recherche));
        long endTime = System.nanoTime();

        return endTime-startTime;
    }

    public static double seq (String [] tab, String recherche, int expected){

        long startTime = System.nanoTime();
        assertEquals(expected, Fouille.fouilleSeq(tab, recherche));
        long endTime = System.nanoTime();

        return endTime-startTime;
    }

    @BeforeAll
    public static void initAll() {
        String[] tab = {"0","1","2","3","4","5"};
        String recherche = "4";

        int result = Fouille.fouilleSeq(tab, recherche);
        System.out.println("Exécution inutile: " + result );
    }

    @BeforeEach
    public void initEach() {
        System.gc();
        long test = System.nanoTime();
        System.out.println("Garbage collector launched @" + test);
    }


    @Test
    public void tab1(){
        String[] tab = {"0","1","2","3","4","5"};
        String recherche = "4";

        System.out.println("-----------------------------------------------------------");
        System.out.println("Temps d'exécution dicho: " + dicho(tab, recherche, 4) + " ns");
        System.out.println("Temps d'exécution seq: " + seq(tab, recherche, 4) + " ns");
        System.out.println("-----------------------------------------------------------");
    }


    @Test
    public void tab2() {
        String[] tab = new String[10000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = genereChaine();
        }

        Arrays.sort(tab);

        System.out.println("-----------------------------------------------------------");
        System.out.println("Temps d'exécution dicho: " + dicho(tab, tab[0], 0) + " ns");
        System.out.println("Temps d'exécution seq: " + seq(tab, tab[0], 0) + " ns");
        System.out.println("-----------------------------------------------------------");
    }

    @Test
    public void tab3() {
        String[] tab = new String[10000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = genereChaine();
        }

        Arrays.sort(tab);

        System.out.println("-----------------------------------------------------------");
        System.out.println("Temps d'exécution dicho: " + dicho(tab, tab[5002], 5002) + " ns");
        System.out.println("Temps d'exécution seq: " + seq(tab, tab[5002], 5002) + " ns");
        System.out.println("-----------------------------------------------------------");
    }

    @Test
    public void tab4() {
        String[] tab = new String[10000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = genereChaine();
        }

        Arrays.sort(tab);

        System.out.println("-----------------------------------------------------------");
        System.out.println("Temps d'exécution dicho: " + dicho(tab, tab[9999], 9999) + " ns");
        System.out.println("Temps d'exécution seq: " + seq(tab, tab[9999], 9999) + " ns");
        System.out.println("-----------------------------------------------------------");
    }

    @Test
    public void tab5() {
        String[] tab = new String[10000];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = genereChaine();
        }

        Arrays.sort(tab);

        System.out.println("-----------------------------------------------------------");
        System.out.println("Temps d'exécution dicho: " + dicho(tab, "afsdlkajfdslkfsdjalafjsdkasfd", -1) + " ns");
        System.out.println("Temps d'exécution seq: " + seq(tab, "afsdlkajfdslkfsdjalafjsdkasfd", -1) + " ns");
        System.out.println("-----------------------------------------------------------");
    }


}
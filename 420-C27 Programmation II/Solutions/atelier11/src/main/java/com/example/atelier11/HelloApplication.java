package com.example.atelier11;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class HelloApplication extends Application {

    public static String genereChaine() {
        final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        String result = "";
        int index;

        for (int i = 0; i < 10; i++) {
            index = random.nextInt(ALPHA.length());
            result += ALPHA.charAt(index);
        }

        return result;
    }

    @Override
    public void start(Stage stage) throws IOException {
        //Variables globales
        final int NB = 500;
        String[] tableauCh = new String[NB];
        String[] tableauChSort = new String[NB];

        for (int i=0; i<NB; i++){
            String ch = genereChaine();
            tableauCh[i] = ch;
            tableauChSort[i] = ch;
        }

        //Méthode pour générer une chaîne de caractères minuscules de longueur 10




        HBox hbox = new HBox();

        ListView<String> listview = new ListView<>();

        for (int i=0; i<NB; i++) {
            listview.getItems().add(tableauCh[i]);
        }

        Arrays.sort(tableauChSort);
        ListView<String> listviewSort = new ListView<>();

        for (int i=0; i<NB; i++) {
            listviewSort.getItems().add(tableauChSort[i]);
        }

        VBox vbox = new VBox();

        vbox.setAlignment(Pos.CENTER);
        TextField txt_Name = new TextField();
        Button btnFouiller = new Button("Fouiller");
        Label lblSeq = new Label("Fouille Séquentielle");
        TextField txt_Seq = new TextField();
        Label lblDic = new Label("Fouille Dichotomique");
        TextField txt_Dic= new TextField();

        vbox.getChildren().addAll(txt_Name, btnFouiller, lblSeq, txt_Seq, lblDic, txt_Dic);

        listview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                txt_Name.setText(newValue);
            }
        });

        btnFouiller.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Bout de code pour calculer le temps d'exécution
                long tempsDebut, tempsFin, duree;

                tempsDebut = System.nanoTime();

                Fouille.fouilleSeq(tableauCh, txt_Name.getText());

                tempsFin = System.nanoTime();
                duree = tempsFin-tempsDebut;

                txt_Seq.setText(String.valueOf(duree));

                tempsDebut = System.nanoTime();

                Fouille.fouilleDichotomique(tableauChSort, txt_Name.getText());

                tempsFin = System.nanoTime();
                duree = tempsFin-tempsDebut;

                txt_Dic.setText(String.valueOf(duree));
            }
        });


        hbox.getChildren().addAll(listview, listviewSort, vbox);

        Scene scene = new Scene(hbox, 640, 480);
        stage.setTitle("Hello there!!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
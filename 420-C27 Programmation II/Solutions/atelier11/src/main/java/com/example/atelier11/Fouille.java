package com.example.atelier11;

public class Fouille {



    public static int fouilleDichotomique(String[] tableau, String valeur){
        int debut = 0;
        int fin = tableau.length - 1;
        int milieu = 0;
        boolean trouve = false;

        while (debut <= fin && !trouve){
            milieu = (debut + fin) / 2;

            if (tableau[milieu].compareTo(valeur) == 0)
                trouve = true;
            else if (tableau[milieu].compareTo(valeur) > 0)
                fin = milieu - 1;
            else
                debut = milieu + 1;
        }

        if (trouve)
            return milieu;
        else
            return -1;
    }

    public static int fouilleSeq(String [] tableau, String recherche){
        for (int i = 0; i < tableau.length; i++)
            if (recherche.equals(tableau[i]))
                return i;

        return -1;
    }
}
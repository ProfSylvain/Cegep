package com.example.atelier6;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Random;

public class HelloApplication extends Application {

    private static int nbrEssai;

    @Override
    public void start(Stage stage) throws IOException {

        VBox vb = new VBox();
        vb.setAlignment(Pos.CENTER);
        vb.setSpacing(10);

        GridPane gp = new GridPane();

        gp.setHgap(20);
        gp.setVgap(20);
        gp.setAlignment(Pos.CENTER);


        Label lbl = new Label();
        lbl.setText("Bonjour");
        lbl.setAlignment(Pos.CENTER);
        lbl.prefWidth(500);

        Random r = new Random();
        int toFind = r.nextInt(100) + 1;


        EventHandler<ActionEvent> clickButton = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (actionEvent.getTarget() instanceof Button) {
                    Button btn = (Button) actionEvent.getTarget();
                    btn.setDisable(true);
                    nbrEssai += 1;

                    int chosen = Integer.parseInt(btn.getId());

                    if (chosen == toFind) {
                        lbl.setText("Bravo");
                    } else if (chosen < toFind) {
                        lbl.setText(chosen + ": trop bas");
                    } else {
                        lbl.setText(chosen + ": trop haut");
                    }
                }
            }
        };

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Button btn = new Button();
                btn.setId(String.valueOf(i * 10 + j + 1));
                btn.setText(String.valueOf(i * 10 + j + 1));
                gp.add(btn, j, i);
                btn.setOnAction(clickButton);
            }
        }

        vb.getChildren().addAll(gp, lbl);


        Scene scene = new Scene(vb, 640, 480);
        stage.setTitle("Hello there!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
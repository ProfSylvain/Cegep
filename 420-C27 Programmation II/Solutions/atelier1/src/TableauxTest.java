import org.junit.Assert;
import org.junit.Test;


public class TableauxTest {

    @Test
    public void trierCroissantSSS1(){
        int[] a = new int[]{3,4,5};
        Tableaux.trierCroissantSSS(a);

        Assert.assertArrayEquals(new int[]{3,4,5}, a);
    }

    @Test
    public void trierCroissantSSS2(){
        int[] a = new int[]{5,4,3,2,1};
        Tableaux.trierCroissantSSS(a);

        Assert.assertArrayEquals(new int[]{1,2,3,4,5}, a);
    }

    @Test
    public void trierCroissantSSS3(){
        int[] a = new int[]{1,4,9,2,1};
        Tableaux.trierCroissantSSS(a);

        Assert.assertArrayEquals(new int[]{1,1,2,4,9}, a);
    }

    @Test
    public void trierCroissantSSS4(){
        int[] a = new int[]{1,4,-9,2,1};
        Tableaux.trierCroissantSSS(a);

        Assert.assertArrayEquals(new int[]{-9,1,1,2,4}, a);
    }

    @Test
    public void trierDecroissantSSS1(){
        int[] a = new int[]{3,4,5};
        Tableaux.trierDecroissantSSS(a);

        Assert.assertArrayEquals(new int[]{5,4,3}, a);
    }

    @Test
    public void trierDecroissantSSS2(){
        int[] a = new int[]{5,4,3,2,1};
        Tableaux.trierDecroissantSSS(a);

        Assert.assertArrayEquals(new int[]{5,4,3,2,1}, a);
    }

    @Test
    public void trierDecroissantSSS3(){
        int[] a = new int[]{1,4,9,2,1};
        Tableaux.trierDecroissantSSS(a);

        Assert.assertArrayEquals(new int[]{9,4,2,1,1}, a);
    }
    @Test
    public void trierCroissantSSSS1(){
        String[] a = new String[]{"5", "4", "3", "2", "1"};
        Tableaux.trierCroissantSSS(a);
        Assert.assertArrayEquals(new String[]{"1", "2", "3", "4", "5"}, a);
    }

    @Test
    public void trierCroissantSSSS2(){
        String[] a = new String[]{"3","4","5","2","1"};
        Tableaux.trierCroissantSSS(a);

        Assert.assertArrayEquals(new String[]{"1","2","3","4","5"}, a);
    }

    @Test
    public void trierCroissantSSSS3(){
        String[] a = new String[]{"1","4","9","2","1"};
        Tableaux.trierCroissantSSS(a);

        Assert.assertArrayEquals(new String[]{"1","1","2","4","9"}, a);
    }

    @Test
    public void trierDecroissantSSSS1(){
        String[] a = new String[]{"5", "4", "3", "2", "1"};
        Tableaux.trierCroissantSSS(a);
        Assert.assertArrayEquals(new String[]{"1", "2", "3", "4", "5"}, a);
    }

    @Test
    public void trierDecroissantSSSS2(){
        String[] a = new String[]{"1","4","5","2","3"};
        Tableaux.trierDecroissantSSS(a);

        Assert.assertArrayEquals(new String[]{"5","4","3","2","1"}, a);
    }

    @Test
    public void trierDecroissantSSSS3(){
        String[] a = new String[]{"1","4","9","2","1"};
        Tableaux.trierDecroissantSSS(a);

        Assert.assertArrayEquals(new String[]{"9","4","2","1","1"}, a);
    }
    @Test
    public void maximum1(){
        int[] a = new int[]{1,2,3,4,5};
        Assert.assertEquals(5, Tableaux.maximum(a));
    }

    @Test
    public void maximum2(){
        int[] a = new int[]{-1000,1,2,3,4,5};
        Assert.assertEquals(5, Tableaux.maximum(a));
    }

    @Test
    public void minimum1(){
        int[] a = new int[]{1,2,3,4,5};
        Assert.assertEquals(1, Tableaux.minimum(a));
    }

    @Test
    public void minimum2(){
        int[] a = new int[]{1,2,3,4,5, -1000};
        Assert.assertEquals(-1000, Tableaux.minimum(a));
    }



    @Test
    public void moyenne1(){
        int[] a = new int[]{1,2,3,4,5};
        Assert.assertEquals(15/5, Tableaux.moyenne(a),0.01);
    }

    @Test
    public void fouille1(){
        int[] a = new int[]{1,2,3,4,5};
        Assert.assertEquals(4, Tableaux.fouilleSeq(a,5));
    }

    @Test
    public void fouille2(){
        int[] a = new int[]{1,2,3,4,5};
        Assert.assertEquals(0, Tableaux.fouilleSeq(a,1));
    }

    @Test
    public void fouille3(){
        int[] a = new int[]{1,2,3,4,5};
        Assert.assertEquals(-1, Tableaux.fouilleSeq(a,-5));
    }

}
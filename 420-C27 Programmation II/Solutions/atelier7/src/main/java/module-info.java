module com.example.atelier7 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.atelier7 to javafx.fxml;
    exports com.example.atelier7;
}
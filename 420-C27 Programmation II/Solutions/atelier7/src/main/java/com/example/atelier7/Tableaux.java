package com.example.atelier7;

/**
 * @author France Beaudoin
 */
public class Tableaux {
    private static void permute(int[] tab, int i1, int i2){
        int transit = tab[i1];
        tab[i1] = tab[i2];
        tab[i2] = transit;
    }

    private static void permute(String[] tab, int i1, int i2){
        String transit = tab[i1];
        tab[i1] = tab[i2];
        tab[i2] = transit;
    }

    public static void trierCroissantSSS(int[] tab){
        int imin;
        for (int i=0; i<tab.length-1; i++){
            imin = i;
            for (int j=i+1; j<tab.length; j++){
                if (tab[j] < tab[imin])
                    imin = j;
            }
            if (imin != i)
                permute(tab, i, imin);
        }
    }

    public static void trierCroissantSSS(String[] tab){
        int imin;
        for (int i=0; i<tab.length-1; i++){
            imin = i;
            for (int j=i+1; j<tab.length; j++){
                if (Integer.parseInt(tab[j]) < Integer.parseInt(tab[imin]))
                    imin = j;
            }
            if (imin != i)
                permute(tab, i, imin);
        }
    }

    public static void trierDecroissantSSS(int[] tab){
        int imax;
        for (int i=0; i<tab.length-1; i++){
            imax = i;
            for (int j=i+1; j<tab.length; j++){
                if (tab[j] > tab[imax])
                    imax = j;
            }
            if (imax != i)
                permute(tab, i, imax);
        }
    }

    public static void trierDecroissantSSS(String[] tab){
        int imax;
        for (int i=0; i<tab.length-1; i++){
            imax = i;
            for (int j=i+1; j<tab.length; j++){
                if (Integer.parseInt(tab[j]) > Integer.parseInt(tab[imax]))
                    imax = j;
            }
            if (imax != i)
                permute(tab, i, imax);
        }
    }

    public static int maximum(int[] tab){
        int max = tab[0];
        for (int i=1; i<tab.length; i++)
            if (tab[i] > max)
                max = tab[i];
        return max;
    }

    public static int maximum(String[] tab){
        int max = Integer.parseInt(tab[0]);
        for (int i=1; i<tab.length; i++)
            if (Integer.parseInt(tab[i]) > max)
                max = Integer.parseInt(tab[i]);
        return max;
    }

    public static int minimum(int[] tab){
        int min = tab[0];
        for (int i=1; i<tab.length; i++)
            if (tab[i] < min)
                min = tab[i];
        return min;
    }

    public static int minimum(String[] tab){
        int min = Integer.parseInt(tab[0]);
        for (int i=1; i<tab.length; i++)
            if (Integer.parseInt(tab[i]) < min)
                min = Integer.parseInt(tab[i]);
        return min;
    }

    public static double moyenne(int[] tab){
        double moyenne = 0;
        double somme = 0.0;
        if (tab.length >0) {
            for (int i = 0; i < tab.length; i++)
                somme += tab[i];
            moyenne = somme / tab.length;
        }
        return moyenne;
    }

    public static double moyenne(String[] tab){
        double moyenne = 0;
        double somme = 0.0;
        if (tab.length >0) {
            for (int i = 0; i < tab.length; i++)
                somme += Integer.parseInt(tab[i]);
            moyenne = somme / tab.length;
        }
        return moyenne;
    }

    public static int fouilleSeq(int[] tab, int valeur){
        boolean trouve = false;
        int i = 0;

        while(i < tab.length && !trouve){
            if (tab[i] == valeur)
                trouve = true;
            else
                i++;
        }

        if (trouve)
            return i;
        else
            return -1;
    }

    public static int fouilleSeq(String[] tab, String valeur){
        boolean trouve = false;
        int i = 0;

        while(i < tab.length && !trouve){
            if (tab[i].equals(valeur))
                trouve = true;
            else
                i++;
        }

        if (trouve)
            return i;
        else
            return -1;
    }


}

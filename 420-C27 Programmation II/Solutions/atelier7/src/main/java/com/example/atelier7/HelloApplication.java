package com.example.atelier7;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Random;

public class HelloApplication extends Application {

    public static String[] returnXIntRandomElements(int x) {

        String[] tableaux = new String[x];
        Random r = new Random();

        for (int i = 0; i < x; i++){
            tableaux[i] = String.valueOf(r.nextInt(Integer.MAX_VALUE - 1));
        }
        return tableaux;
    }

    @Override
    public void start(Stage stage) throws IOException {
        HBox hbox = new HBox();
        VBox vbox_left = new VBox();
        VBox vbox_right = new VBox();

        vbox_left.minWidth(320);
        vbox_left.setPadding(new Insets(20));
        vbox_left.setSpacing(5);
        vbox_left.setAlignment(Pos.CENTER);
        vbox_right.minWidth(320);
        vbox_right.setPadding(new Insets(20));

        hbox.getChildren().addAll(vbox_left,vbox_right);

        TextArea txt = new TextArea();
        txt.setPrefRowCount(100);
        vbox_right.getChildren().add(txt);

        ComboBox<String> cmb = new ComboBox<>();
        cmb.getItems().addAll("Vider", "Aléatoire", "Nombre 1 à 100", "Nombre -50 à 50" );

        RadioButton rbCroissant = new RadioButton();
        RadioButton rbDecroissant = new RadioButton();
        ToggleGroup tgTri = new ToggleGroup();

        rbCroissant.setText("Croissant");
        rbDecroissant.setText("Décroissant");

        rbCroissant.setToggleGroup(tgTri);
        rbDecroissant.setToggleGroup(tgTri);

        Button btnTrier = new Button();
        btnTrier.setText("Trier");

        Label txtMin = new Label();
        Label txtMax = new Label();
        Label txtMoy = new Label();

        txtMin.setText("0");
        txtMax.setText("0");
        txtMoy.setText("0");

        TextField txtFind = new TextField();
        Button btnFind = new Button();
        btnFind.setText("Rechercher");

        Label txtPosition = new Label();

        ChangeListener<String> textAreaListener = new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if (t1 != "") {
                    String[] data = txt.getText().split("\n");
                    txtMin.setText(String.format("Min: %d", Tableaux.minimum(data)));
                    txtMax.setText(String.format("Max: %d", Tableaux.maximum(data)));
                    txtMoy.setText(String.format("Moy: %9.0f", Tableaux.moyenne(data)));
                }
                else {
                    txtMin.setText("Min: Aucune donnée");
                    txtMax.setText("Max: Aucune donnée");
                    txtMoy.setText("Moy: Aucune donnée");
                }
            }
        };

        cmb.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                txt.textProperty().removeListener(textAreaListener);
                String[] data = new String[100];

                switch (cmb.getSelectionModel().getSelectedItem()) {
                    case "Vider":
                        txt.clear();
                        break;
                    case "Aléatoire":
                        data = returnXIntRandomElements(100);
                        break;
                    case "Nombre 1 à 100":                        
                        data = new String[100];
                        for (int i = 0; i < 100; i++) {                            
                            data[i] = String.valueOf(i+1);
                        }
                        break;
                    case "Nombre -50 à 50":                        
                        data = new String[101];
                        for (int i = 0; i < 101; i++) {                            
                            data[i] = String.valueOf(i-50);
                        }
                        break;
                }
                txt.textProperty().addListener(textAreaListener);
                txt.setText(String.join("\n", data));
            }
        });

        btnTrier.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                txt.textProperty().removeListener(textAreaListener);
                String[] data = txt.getText().split("\n");
                txt.clear();

                if (rbCroissant.isSelected())
                     Tableaux.trierCroissantSSS(data);
                else if (rbDecroissant.isSelected())
                    Tableaux.trierDecroissantSSS(data);

                txt.textProperty().addListener(textAreaListener);
                txt.setText(String.join("\n", data));
            }
        });

        btnFind.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                String[] data = txt.getText().split("\n");
                String toFind = txtFind.getText();

                if (toFind != "") {
                    int position = Tableaux.fouilleSeq(data, toFind);
                    if (position >= 0 )
                        txtPosition.setText(String.format("Ligne: %d", position));
                    else
                        txtPosition.setText("Introuvable");
                }
            }
        });

        vbox_left.getChildren().addAll(cmb, rbCroissant, rbDecroissant, btnTrier, txtMin, txtMax, txtMoy, txtFind, btnFind, txtPosition);


        Scene scene = new Scene(hbox, 640, 480);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
module com.example.atelier8 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.atelier8 to javafx.fxml;
    exports com.example.atelier8;
}
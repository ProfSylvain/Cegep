package com.example.atelier5;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {

        VBox vb = new VBox();
        HBox hbox_line1 = new HBox();
        HBox hbox_line2 = new HBox();
        HBox hbox_line3 = new HBox();

        // Appeareance
        hbox_line1.setSpacing(20);
        hbox_line1.setAlignment(Pos.CENTER);
        hbox_line2.setSpacing(20);
        hbox_line2.setAlignment(Pos.CENTER);
        hbox_line3.setSpacing(20);
        hbox_line3.setAlignment(Pos.CENTER);


        Label lbl = new Label("Ajouter un repas");
        TextField tf = new TextField();
        Button btn = new Button("Ajouter");
        ListView<String> lv = new ListView<String>();

        btn.setDefaultButton(true);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Text txt = new Text(tf.getText());
                hbox_line1.getChildren().add(txt);
                tf.setText("");

                txt.setOnDragDetected(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event) {
                        /* drag was detected, start a drag-and-drop gesture*/
                        /* allow any transfer mode */
                        Dragboard db = txt.startDragAndDrop(TransferMode.MOVE);

                        /* Put a string on a dragboard */
                        ClipboardContent content = new ClipboardContent();
                        content.putString(txt.getText());
                        db.setContent(content);

                        event.consume();
                    }
                });

                txt.setOnDragDone(new EventHandler<DragEvent>() {
                    public void handle(DragEvent event) {
                        /* the drag and drop gesture ended */
                        /* if the data was successfully moved, clear it */
                        if (event.getTransferMode() == TransferMode.MOVE) {
                            hbox_line1.getChildren().remove(txt);
                        }
                        event.consume();
                    }
                });
            }
        });

        VBox.setMargin(hbox_line1, new Insets(30,0,0,0));
        VBox.setMargin(hbox_line2, new Insets(20,0,0,0));
        VBox.setMargin(hbox_line3, new Insets(20,0,0,0));


        hbox_line2.getChildren().addAll(lbl, tf, btn);
        hbox_line3.getChildren().add(lv);
        vb.getChildren().addAll(hbox_line1,hbox_line2, hbox_line3);


        Scene scene = new Scene(vb, 1024, 768);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();



        lv.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                /* drag was detected, start a drag-and-drop gesture*/
                /* allow any transfer mode */
                Dragboard db = lv.startDragAndDrop(TransferMode.MOVE);

                /* Put a string on a dragboard */
                ClipboardContent content = new ClipboardContent();
                content.putString(String.valueOf(lv.getSelectionModel().getSelectedIndex()));
                db.setContent(content);

                event.consume();
            }
        });

        lv.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data is dragged over the target */
                /* accept it only if it is not dragged from the same node
                 * and if it has a string data */
                if (event.getGestureSource() != vb &&
                        event.getDragboard().hasString()) {
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(TransferMode.MOVE);
                }

                event.consume();
            }
        });

        lv.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data dropped */
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString()) {
                    lv.getItems().add(db.getString());
                    success = true;

                }
                /* let the source know whether the string was successfully
                 * transferred and used */
                event.setDropCompleted(success);

                event.consume();
            }
        });

        scene.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data is dragged over the target */
                /* accept it only if it is not dragged from the same node
                 * and if it has a string data */
                if (event.getGestureSource() == lv &&
                        event.getDragboard().hasString()) {
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(TransferMode.MOVE);
                }

                event.consume();
            }
        });

        scene.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* data dropped */
                /* if there is a string data on dragboard, read it and use it */
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasString()) {
                    String lvItem = lv.getItems().get(Integer.parseInt(db.getString()));
                    lv.getItems().remove(Integer.parseInt(db.getString()));

                    Text txt = new Text(db.getString());
                    hbox_line1.getChildren().add(lvItem.);
                    success = true;
                }
                /* let the source know whether the string was successfully
                 * transferred and used */
                event.setDropCompleted(success);

                event.consume();
            }
        });





    }



    public static void main(String[] args) {
        launch();
    }
}
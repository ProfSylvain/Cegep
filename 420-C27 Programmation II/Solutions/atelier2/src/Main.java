import java.util.Scanner;

public class Main {


    public static double division(int a, int b) throws ArithmeticException {
        if (b == 0)
            throw new ArithmeticException();
        return (double) a/b;
    }
    public static void main(String[] args) {



        int firstNumber = getNumber();
        int secondNumber = getNumber();

        try {
            System.out.format("La division donne: %f", division(firstNumber, secondNumber));
        }
        catch (ArithmeticException e){
            System.out.println("Une division par 0 est survenue");
        }

    }

    private static int getNumber() {
        Scanner sc = new Scanner(System.in);
        boolean inputOk = false;
        int number = 0;

        while (!inputOk) {
            System.out.println("Entrez un nombre:");
            if (sc.hasNextInt()) {
                inputOk = true;
                number = sc.nextInt();
            }
            else sc.next();
        }
        //sc.close();

        return number;
    }
}
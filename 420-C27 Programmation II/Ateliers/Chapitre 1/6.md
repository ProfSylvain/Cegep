# Le détecteur de mensonge réinventé

Le but de l'atelier aujourd'hui est de créer un petit jeu d'essai erreur avec rétroaction.

Vous devez créer une interface de 100 boutons, numérotés de 1 à 100 comme l'exemple suivant:

![](images/SpeakYourMind.png)

* À chaque tour, vous devez choisir un bouton.
* Un message dans un label au bas vous indique si votre réponse est bonne, au-dessus ou en deça de la valeur choisi par le système au démarrage de l'application.
* Vous avez 8 possibilités avant que l'application ne vous kick out ;)
* Un coup sélectionné, un bouton devient non sélectable jusqu'à une nouvelle partie.
* À la fin, une boîte de dialogue vous demande si vous souhaitez rejouer ou non. Vous n'avez pas à implémenter ce qu'il faut pour rejouer.
* Des heures de plaisir en famille

